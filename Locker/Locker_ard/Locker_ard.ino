#include<SoftwareSerial.h>
SoftwareSerial GSM(9, 10); //(Tx,Rx)

int ledPin = 13;
int flushtime = 5000;
int timeout;
String buffer;

void setup()
{
  Serial.begin(9600);
  GSM.begin(9600);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
  Serial.println("test serial");
}

void loop()
{
  Serial.println("in loop");
  delay(10);
  //digitalWrite(ledPin, LOW);
  GPRSserverUpdate();
}



void GPRSserverUpdate()
{
  Serial.println("1");
  ATinitiation();
  Serial.println("2");
  CIPSHUT();
  Serial.println("3");
  CIPMUX();
  Serial.println("4");
  CGATT();
  Serial.println("5");
  CSTT();
  Serial.println("6");
  CIICR();
  Serial.println("7");
  CIFSR();
  Serial.println("8");
  CIPSTART();
  Serial.println("9");
  CIPSEND();
  Serial.println("10");
  return;
}


String readSerial()
{
  timeout = 0;
  while  (!GSM.available() && timeout < 12000)
  {
    delay(13);
    timeout++;
  }

  if (GSM.available())
  {
    return GSM.readString();
  }
}


void flushReceive(long time)
{
  int time_start = millis();
  while (GSM.available() > 0 || (millis() - time_start < time))
  {
    delay(10);
    if (GSM.available() > 0)
    {
      char c = GSM.read();
      if (c == '\r' || c == '\n')
      {
        continue;
      }
    }
  }
}


bool ATinitiation()
{
  Serial.println("inside CIPSHUT");
  delay(10);
  String ATcheck;

  for (int l = 0; l < 6; l++)
  {
    GSM.print("AT\r");
    delay(500);
    ATcheck = readSerial();
    Serial.println(ATcheck);
    delay(10);

    if ((ATcheck.indexOf("ER")) == -1)
    {
      //Serial.println("no ERR in ATinitiation");
      //delay(10);
      return true;
    }

  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}


bool CIPSHUT()
{
  int waitTime = 500;
  int addDelay = 0;
  Serial.println("inside CIPSHUT");
  delay(10);
  String CIPSHUTcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    GSM.print("AT+CIPSHUT\r\n");

    waitTime = waitTime + addDelay;

    Serial.println("waitTimeCIPSHUT = ");
    Serial.println(waitTime);

    if (waitTime > 5000)
    {
      loop();
    }

    while (waitTime > 0)
    {
      delay(1);
      waitTime--;
    }

    addDelay = addDelay + 1000;

    CIPSHUTcheck = readSerial();
    Serial.println(CIPSHUTcheck);
    delay(10);

    if ((CIPSHUTcheck.indexOf("ER")) == -1)
    {
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }

  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}


bool CIPMUX()
{
  int waitTime = 500;
  int addDelay = 0;
  Serial.println("inside CIPMUX");
  delay(10);
  String CIPMUXcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    GSM.print("AT+CIPMUX=0\r\n");

    waitTime = waitTime + addDelay;

    Serial.println("waitTimeCIPMUX = ");
    Serial.println(waitTime);

    if (waitTime > 5000)
    {
      loop();
    }

    while (waitTime > 0)
    {
      delay(1);
      waitTime--;
    }

    addDelay = addDelay + 1000;

    CIPMUXcheck = readSerial();
    Serial.println(CIPMUXcheck);
    delay(10);

    if ((CIPMUXcheck.indexOf("ER")) == -1)
    {
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }

  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}


bool CGATT()
{
  int waitTime = 500;
  int addDelay = 0;
  Serial.println("inside CGATT");
  delay(10);
  String CGATTcheck;

  for (int l = 0; l < 6; l++)
  {
    GSM.print("AT+CGATT=1\r\n");

    waitTime = waitTime + addDelay;

    Serial.println("waitTimeCGATT = ");
    Serial.println(waitTime);

    if (waitTime > 8000)
    {
      loop();
    }
    while (waitTime > 0)
    {
      delay(1);
      waitTime--;
    }

    addDelay = addDelay + 1500;

    CGATTcheck = readSerial();
    Serial.println(CGATTcheck);
    delay(10);

    if ((CGATTcheck.indexOf("ER")) == -1)
    {
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }

  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}


bool CSTT()
{
  int waitTime = 500;
  int addDelay = 0;
  Serial.println("inside CSTT");
  delay(10);
  String CSTTcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    GSM.print("AT+CSTT\r");

    waitTime = waitTime + addDelay;

    Serial.println("waitTimeCSTT = ");
    Serial.println(waitTime);

    if (waitTime > 5000)
    {
      loop();
    }

    while (waitTime > 0)
    {
      delay(1);
      waitTime--;
    }

    addDelay = addDelay + 1000;

    CSTTcheck = readSerial();
    Serial.println(CSTTcheck);
    delay(10);

    if ((CSTTcheck.indexOf("ER")) == -1)
    {
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }

  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}


bool CIICR()
{
  int waitTime = 500;
  int addDelay = 0;
  Serial.println("inside CIICR");
  delay(10);
  String CIICRcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    GSM.print("AT+CIICR\r");

    waitTime = waitTime + addDelay;

    Serial.println("waitTimeCIICR = ");
    Serial.println(waitTime);

    if (waitTime > 10000)
    {
      loop();
    }

    while (waitTime > 0)
    {
      delay(1);
      waitTime--;
    }

    addDelay = addDelay + 2000;

    CIICRcheck = readSerial();
    Serial.println(CIICRcheck);
    delay(10);

    if ((CIICRcheck.indexOf("ER")) == -1)
    {
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }

  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}


bool CIFSR()
{
  int waitTime = 500;
  int addDelay = 0;
  Serial.println("inside CIFSR");
  delay(10);
  String CIFSRcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    GSM.print("AT+CIFSR\r");

    waitTime = waitTime + addDelay;

    Serial.println("waitTimeCIFSR = ");
    Serial.println(waitTime);

    if (waitTime > 5000)
    {
      loop();
    }

    while (waitTime > 0)
    {
      delay(1);
      waitTime--;
    }

    addDelay = addDelay + 1000;

    CIFSRcheck = readSerial();
    Serial.println(CIFSRcheck);
    delay(10);

    if ((CIFSRcheck.indexOf("ER")) == -1)
    {
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }
  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}


bool CIPSTART()
{
  int waitTime = 500;
  int addDelay = 0;
  Serial.println("inside CIPSTART");
  delay(10);
  String CIPSTARTcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    GSM.print("AT+CIPSTART=\"TCP\",\"207.154.244.63\",\"80\"\r\n\r");

    waitTime = waitTime + addDelay;

    Serial.println("waitTimeCIPSTART = ");
    Serial.println(waitTime);

    if (waitTime > 8000)
    {
      loop();
    }

    while (waitTime > 0)
    {
      delay(1);
      waitTime--;
    }

    addDelay = addDelay + 2500;

    CIPSTARTcheck = readSerial();
    Serial.println(CIPSTARTcheck);
    delay(10);

    if ((CIPSTARTcheck.indexOf("ER")) == -1)
    {
      if ((CIPSTARTcheck.indexOf("CONNECT")) != -1)
      {
        //Serial.println("inside ATinitiation if");
        //delay(500);

        return true;
      }
    }

    if ((CIPSTARTcheck.indexOf("ALREADY")) != -1)
    {
      return true;
    }

  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}


bool CIPSEND()
{
  int waitTime = 500;
  int addDelay = 0;
  Serial.println("inside CIPSEND");
  delay(10);
  String CIPSENDcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);

    GSM.print("AT+CIPSEND=40\r\n");

    waitTime = waitTime + addDelay;

    Serial.println("waitTimeCIPSEND = ");
    Serial.println(waitTime);

    if (waitTime > 6000)
    {
      loop();
    }

    while (waitTime > 0)
    {
      delay(1);
      waitTime--;
    }

    addDelay = addDelay + 2000;

    CIPSENDcheck = readSerial();

    if ((CIPSENDcheck.indexOf("ER")) == -1)
    {
      GSM.print("GET /pump-status/giriwar/pass12/1/1/2");

      GSM.print("\r\n");
      delay(1000);

      GSM.print(0x1a);

      buffer = readSerial();

      if (buffer.indexOf("ON") != -1)
      {
        Serial.println("in ON");
        digitalWrite(ledPin, HIGH);
      }

      if (buffer.indexOf("OFF") != -1)
      {
        Serial.println("in OFF");
        digitalWrite(ledPin, LOW);
      }

      return true;
    }
  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}


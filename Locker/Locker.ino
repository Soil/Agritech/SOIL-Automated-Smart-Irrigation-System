int flushtime = 5000;
int timeout;
char MyByte[3];
int cc, v, t;
String buffer;

void setup()
{
  Serial.begin(9600);
  Serial1.begin(9600);

  Serial.println("test serial");
}

void loop()
{
  Serial.println("in loop");
  delay(10);
  //GPRSserverUpdate();
  gprsStart();
  switchStatus();
}


String readSerial()
{
  timeout = 0;

  while  (!Serial1.available() && timeout < 12000  )
  {
    delay(13);
    timeout++;
  }


  if (Serial1.available())
  {
    return Serial1.readString();
  }

}


void flushReceive(long time)
{
  digitalWrite(GREEN_LED, HIGH);
  int time_start = millis();
  while (Serial1.available() > 0 || (millis() - time_start < time))
  {
    delay(10);
    if (Serial1.available() > 0)
    {
      char c = Serial1.read();
      if (c == '\r' || c == '\n')
      {
        continue;
      }
    }
  }
  digitalWrite(GREEN_LED, LOW);
}



void GPRSserverUpdate()
{
  //Serial.println("in GPRSserverUpdate");
  //delay(10000);

  Serial1.print("AT\r\n");
  delay(4000);
  Serial.println(readSerial());

  Serial1.print("AT+CIPSHUT\r\n");
  delay(4000);
  Serial.println(readSerial());


  Serial1.print("AT+CIPMUX=0\r\n");
  delay(4000);
  Serial.println(readSerial());

  Serial1.print("AT+CGATT=1\r\n");
  delay(4000);
  Serial.println(readSerial());

  Serial1.print("AT+CSTT=\"www\",\"\",\"\"\r\n");
  delay(4000);
  Serial.println(readSerial());

  Serial1.print("AT+CIICR\r\n");
  delay(8000);
  Serial.println(readSerial());

  Serial1.print("AT+CIFSR\r\n");
  delay(4000);
  Serial.println(readSerial());

  Serial1.print("AT+CIPSTART=\"TCP\",\"207.154.244.63\",\"80\"\r\n");
  delay(15000);
  Serial.println(readSerial());

  Serial1.print("AT+CIPSEND=66\r\n");
  delay(4000);
  Serial.println(readSerial());

  Serial1.print("GET /add-sensor-data/giriwar/pass12/1/1/1/");
  delay(5000);



  Serial1.print("000,000,000,000,000");
  delay(1000);

  Serial1.print("/00\r\n");
  delay(1000);

  Serial.println(readSerial());

  Serial1.print(0x1a);
  delay(10000);

  Serial.println(readSerial());
  return;

}



void gprsStart()
{
  delay(10000);

  Serial1.print("AT\r\n");
  delay(500);

  Serial1.print("AT+CIPSHUT\r\n");
  delay(4000);

  Serial1.print("AT+CIPMUX=0\r\n");
  delay(4000);

  Serial1.print("AT+CGATT=1\r\n");
  delay(4000);

  Serial1.print("AT+CSTT=\"www\",\"\",\"\"\r\n");
  delay(4000);

  Serial1.print("AT+CIICR\r\n");
  delay(5000);

  Serial1.print("AT+CIFSR\r\n");
  delay(4000);
  Serial.println(readSerial());
}


void switchStatus()
{
  Serial1.print("AT+CIPSTART=\"TCP\",\"207.154.244.63\",\"80\"\r\n");
  delay(10000);

  Serial1.print("AT+CIPSEND=40\r\n");
  delay(4000);

  Serial1.print("GET /pump-status/giriwar/pass12/1/1/2");

  Serial1.print("\r\n");
  delay(1000);

  Serial1.print(0x1a);

  buffer = readSerial();

  /*
    MyByte[0] = '0'; MyByte[1] = '0';
    cc = 0; v = 0;

    while (v == 0 && cc < 4000)
    {
    Serial.println("in mybyte loop");
    while (Serial1.available() == 0 && cc < 4000) {
      delay(1);  // when no data available at serial port
      cc++;
    }

    MyByte[0] = Serial1.read();
    Serial.println("MyByteinit =");
    delay(10);
    Serial.println(MyByte);
    if (MyByte[0] == 'O')
    {
      MyByte[1] = Serial1.read();
    }

    delay(1);
    cc++;
    }

    Serial.println("MyByte =");
    delay(10);
    Serial.println(MyByte);

    if (MyByte[0] == 'O' && MyByte[1] == 'N')
    {
    Serial.println("in ON loop");
    //digitalWrite(locker_relay, HIGH);
    digitalWrite(RED_LED, LOW);
    digitalWrite(GREEN_LED, HIGH);

    t = 1;
    v = 1;
    }

    if (MyByte[0] == 'O' && MyByte[1] == 'F')
    {
    Serial.println("in OFF loop");
    //digitalWrite(locker_relay, LOW);
    digitalWrite(GREEN_LED, LOW);
    digitalWrite(RED_LED, HIGH);

    t = 0;
    v = 1;
    }
  */

  if (buffer.indexOf("ON") != -1)
  {
    digitalWrite(RED_LED, LOW);
    digitalWrite(GREEN_LED, HIGH);
  }

  if (buffer.indexOf("OFF") != -1)
  {
    digitalWrite(GREEN_LED, LOW);
    digitalWrite(RED_LED, HIGH);
  }
}

int relayPin = 13;
int flushtime = 5000;
int timeout;
String buffer;

void setup()
{
  Serial.begin(9600);
  pinMode(relayPin, OUTPUT);
  delay(10);
  digitalWrite(relayPin, LOW);
}

void loop()
{
  GPRSserverUpdate();
}



void GPRSserverUpdate()
{

  ATinitiation();

  CIPSHUT();

  CIPMUX();

  CGATT();

  CSTT();

  CIICR();

  CIFSR();

  CIPSTART();

  CIPSEND();

  return;
}


String readSerial()
{
  timeout = 0;
  while  (!Serial.available() && timeout < 12000)
  {
    delay(13);
    timeout++;
  }

  if (Serial.available())
  {
    return Serial.readString();
  }
}


void flushReceive(long time)
{
  int time_start = millis();
  while (Serial.available() > 0 || (millis() - time_start < time))
  {
    delay(10);
    if (Serial.available() > 0)
    {
      char c = Serial.read();
      if (c == '\r' || c == '\n')
      {
        continue;
      }
    }
  }
}


bool ATinitiation()
{
  String ATcheck;

  for (int l = 0; l < 6; l++)
  {
    Serial.print("AT\r");
    delay(500);
    ATcheck = readSerial();

    if ((ATcheck.indexOf("ER")) == -1)
    {
      return true;
    }
  }

  loop();
}


bool CIPSHUT()
{
  int waitTime = 500;
  int addDelay = 0;
  String CIPSHUTcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    Serial.print("AT+CIPSHUT\r\n");

    waitTime = waitTime + addDelay;

    if (waitTime > 5000)
    {
      loop();
    }

    while (waitTime > 0)
    {
      delay(1);
      waitTime--;
    }

    addDelay = addDelay + 1000;

    CIPSHUTcheck = readSerial();

    if ((CIPSHUTcheck.indexOf("ER")) == -1)
    {
      return true;
    }

  }

  loop();
}


bool CIPMUX()
{
  int waitTime = 500;
  int addDelay = 0;
  String CIPMUXcheck;

  for (int l = 0; l < 6; l++)
  {
    Serial.print("AT+CIPMUX=0\r\n");

    waitTime = waitTime + addDelay;

    if (waitTime > 5000)
    {
      loop();
    }

    while (waitTime > 0)
    {
      delay(1);
      waitTime--;
    }

    addDelay = addDelay + 1000;

    CIPMUXcheck = readSerial();

    if ((CIPMUXcheck.indexOf("ER")) == -1)
    {
      return true;
    }

  }

  loop();
}


bool CGATT()
{
  int waitTime = 500;
  int addDelay = 0;
  String CGATTcheck;

  for (int l = 0; l < 6; l++)
  {
    Serial.print("AT+CGATT=1\r\n");

    waitTime = waitTime + addDelay;

    if (waitTime > 8000)
    {
      loop();
    }

    while (waitTime > 0)
    {
      delay(1);
      waitTime--;
    }

    addDelay = addDelay + 1500;

    CGATTcheck = readSerial();

    if ((CGATTcheck.indexOf("ER")) == -1)
    {
      return true;
    }

  }

  loop();
}


bool CSTT()
{
  int waitTime = 500;
  int addDelay = 0;
  String CSTTcheck;

  for (int l = 0; l < 6; l++)
  {
    Serial.print("AT+CSTT\r");

    waitTime = waitTime + addDelay;

    if (waitTime > 5000)
    {
      loop();
    }

    while (waitTime > 0)
    {
      delay(1);
      waitTime--;
    }

    addDelay = addDelay + 1000;

    CSTTcheck = readSerial();

    if ((CSTTcheck.indexOf("ER")) == -1)
    {
      return true;
    }

  }

  loop();
}


bool CIICR()
{
  int waitTime = 500;
  int addDelay = 0;
  String CIICRcheck;

  for (int l = 0; l < 6; l++)
  {
    Serial.print("AT+CIICR\r");

    waitTime = waitTime + addDelay;

    if (waitTime > 10000)
    {
      loop();
    }

    while (waitTime > 0)
    {
      delay(1);
      waitTime--;
    }

    addDelay = addDelay + 2000;

    CIICRcheck = readSerial();

    if ((CIICRcheck.indexOf("ER")) == -1)
    {
      return true;
    }

  }

  loop();
}


bool CIFSR()
{
  int waitTime = 500;
  int addDelay = 0;
  String CIFSRcheck;

  for (int l = 0; l < 6; l++)
  {
    Serial.print("AT+CIFSR\r");

    waitTime = waitTime + addDelay;

    if (waitTime > 5000)
    {
      loop();
    }

    while (waitTime > 0)
    {
      delay(1);
      waitTime--;
    }

    addDelay = addDelay + 1000;

    CIFSRcheck = readSerial();

    if ((CIFSRcheck.indexOf("ER")) == -1)
    {
      return true;
    }
  }

  loop();
}


bool CIPSTART()
{
  int waitTime = 500;
  int addDelay = 0;
  String CIPSTARTcheck;

  for (int l = 0; l < 6; l++)
  {
    Serial.print("AT+CIPSTART=\"TCP\",\"207.154.244.63\",\"80\"\r\n\r");

    waitTime = waitTime + addDelay;

    if (waitTime > 8000)
    {
      loop();
    }

    while (waitTime > 0)
    {
      delay(1);
      waitTime--;
    }

    addDelay = addDelay + 2500;

    CIPSTARTcheck = readSerial();

    if ((CIPSTARTcheck.indexOf("ER")) == -1)
    {
      if ((CIPSTARTcheck.indexOf("CONNECT")) != -1)
      {
        return true;
      }
    }

    if ((CIPSTARTcheck.indexOf("ALREADY")) != -1)
    {
      return true;
    }

  }

  loop();
}


bool CIPSEND()
{
  int waitTime = 500;
  int addDelay = 0;
  String CIPSENDcheck;

  for (int l = 0; l < 6; l++)
  {
    Serial.print("AT+CIPSEND=40\r\n");

    waitTime = waitTime + addDelay;

    if (waitTime > 6000)
    {
      loop();
    }

    while (waitTime > 0)
    {
      delay(1);
      waitTime--;
    }

    addDelay = addDelay + 2000;

    CIPSENDcheck = readSerial();

    if ((CIPSENDcheck.indexOf("ER")) == -1)
    {
      Serial.print("GET /pump-status/giriwar/pass12/1/1/2");

      Serial.print("\r\n");
      delay(1000);

      Serial.print(0x1a);

      buffer = readSerial();

      if (buffer.indexOf("ON") != -1)
      {
        digitalWrite(relayPin, HIGH);
      }

      if (buffer.indexOf("OFF") != -1)
      {
        digitalWrite(relayPin, LOW);
      }

      return true;
    }
  }

  loop();
}

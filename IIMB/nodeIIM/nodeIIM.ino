
/*--------------------------Program for Node --------------------------------------------------*/

const int moisturesensor1 = 28;
int moisture1value   = 0;
unsigned int MS1 = 0;
char cmd;
int mydata[3];
char sensID = 'M';


void setup()
{
  Serial1.begin(9600);
  Serial.begin(9600);

  //Serial.println("test serial");
  analogReadResolution(14);
}


/*------------------------Loop function-------------------------------------------------------------*/
void loop()
{
  //checkSerial(sensID);
	sensorRead();
	sdWrite('b');
	delay(10000);
}


/*----------check device ID in serial buffer----------*/
void checkSerial(char deviceID)
{
  Serial.println("in checkSerial function");
  
  if (Serial1.available()) // wait for hardware UART
  {
    cmd = Serial1.read();
    if (cmd == deviceID)
    {
      sensorRead();
    }
  
  }

  //sensorRead();

  while (Serial1.available() == 0)
  {
    //do nothing
  }
  
}


/*-------------------------sensorread function---------------------------------------------------------*/
void sensorRead()
{
  //Serial.println("in sensorRead function");
  
  int i;
  digitalWrite(RED_LED, HIGH);
  //Serial.println("in serialRead");
  //delay(10);

  for (i = 0; i < 3; i++)
  {
    //Serial.println("in s value loop");
    mydata[i] = 's';
  }

  moisture1value = analogRead(moisturesensor1);

  //Serial.println(moisture1value);
  
  MS1 = moisture1value;

  if (MS1 < 6190) {
    MS1 = 6190;
  }

  if (MS1 > 12540) {
    MS1 = 12540;
  }
  //Serial.println(MS1);
  MS1 = (MS1 - 6190) * 10;
  //Serial.println(MS1);
  MS1 = 100 - (MS1 / 635);
  //Serial.println(MS1);

  mydata[0] = MS1 / 100;
  mydata[1] = (MS1 - (mydata[0] * 100)) / 10;
  mydata[2] = MS1 - ((mydata[0] * 100) + mydata[1] * 10 );

  
  digitalWrite(RED_LED, LOW);

  //Serial.println("outside for loop");
  /*
  for (i = 0; i < 3 ; i++)
  {
    //Serial.println("inside for loop");
    //Serial.println("forming data packet");
    //delay(500);
    //Serial.println("i =");
    //Serial.println(i);
    //delay(500);
    Serial1.print(mydata[i]);
    //delay(500);
    //Serial.println("data =");
    //Serial.println(mydata[i]);
    //delay(500);
  }
  */

}


// call this function after mdcMoistSensor/mdcBackMoistSensor have been called
void sdWrite(char deviceID)
{
  //Serial.println("in sdWrite function");
  //delay(10);
  //int realHours = RTChours();

  //int realMinutes = RTCminutes();

  //if (deviceID == 'M') {

    //Serial.print('m');
    //Serial.print(realHours);
    //Serial.print(':');
    //Serial.print(realMinutes);
    //Serial.print(' ');

    //for (int i = 0; i < 3 ; i++)
    //{
      //Serial.print(charMDCmoistValue[i]);
    //}
  //}

  //if (deviceID == 'B') {

    Serial.print('b');
    //Serial.print(realHours);
    //Serial.print(':');
    //Serial.print(realMinutes);
    //Serial.print(' ');
    Serial.print(mydata[0]);
  	Serial.print(mydata[1]);
  	Serial.print(mydata[2]);
  
    //for (int j = 0; j < 3 ; j++)
    //{
      //Serial.print(charMDCbackMoistValue[j]);
    //}
  //}
}

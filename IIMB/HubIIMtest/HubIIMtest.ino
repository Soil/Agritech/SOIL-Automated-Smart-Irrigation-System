
int flushtime = 5000;
int timeout;
char charReceiveByte[4];
char *pointerLoraByte;

const int gsmStatus = 37;
const int gsmPwrkey = 40;

void setup()
{
  Serial.begin(9600);
  Serial1.begin(9600);

  pinMode(gsmStatus, INPUT);
  pinMode(gsmPwrkey, OUTPUT);


  digitalWrite(gsmPwrkey, HIGH);
  delay(3000);
  
  digitalWrite(gsmPwrkey, LOW );
  delay(1000);
  if(digitalRead(gsmStatus) == 1){
     //Serial.println("0");
  }
  flushReceive(flushtime);
  delay(30000);
  //Serial.println("test serial");
  ATinitiation();
  //Serial.println("1");
  QMTOPEN();
  //Serial.println("2");
  QMTCONN();
  //Serial.println("3");
}

void loop()
{
  //Serial.println("in loop");
  //delay(10);
  lora1Ping();
  //Serial.println("t");
  GPRSserverUpdate();
  //Serial.println("4");
  //delay(5000);
  //lora2Ping();
  //GPRSserverUpdate();
  //delay(1000);
  //Serial.println("5");
}



void GPRSserverUpdate()
{
  ATinitiation();
  //Serial.println("init");
  
  QMTPUB();
  //Serial.println("end");
  
  return;
}


String readSerial()
{
  timeout = 0;
  while  (!Serial1.available() && timeout < 12000)
  {
    delay(13);
    timeout++;
  }

  if (Serial1.available())
  {
    return Serial1.readString();
  }
}


void flushReceive(long time)
{
  int time_start = millis();
  while (Serial.available() > 0 || (millis() - time_start < time))
  {
    delay(10);
    if (Serial.available() > 0)
    {
      char c = Serial.read();
      if (c == '\r' || c == '\n')
      {
        continue;
      }
    }
  }
}

bool ATinitiation()
{
  //Serial.println("inside ATinitiation");
  //delay(10);
  String ATcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    Serial1.print("AT\r");
    delay(500);
    ATcheck = readSerial();
    //Serial.println(ATcheck);
    //delay(10);

    if ((ATcheck.indexOf("ER")) == -1)
    {
      if ((ATcheck.indexOf("AT")) != -1)
      {
        //Serial.println("no ERR in ATinitiation");
        //delay(10);

        return true;
      }
    }

  }
  //Serial.println("out of AT");
  //delay(10);
  loop();
}


bool QMTOPEN()
{
  //Serial.println("inside QMTOPEN");
  //delay(10);
  String QMTOPENcheck;

  for (int l = 0; l < 6; l++)
  {
    //Serial.println("QMTOPEN loop");
    //delay(500);
    Serial1.print("AT+QMTOPEN=0,\"104.198.201.218\",1883\r\n");
    //Serial1.print("AT+QMTOPEN=0,\"37.187.106.16\",1883\r\n");
    delay(4000);
    QMTOPENcheck = readSerial();
    //Serial.println(QMTOPENcheck);
    //delay(10);
    //Serial.println("1");

    /*if ((QMTOPENcheck.indexOf("ER")) == -1)
    {
      //Serial.println("QMTOPEN no error");
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }*/
    return true;
    //Serial.println("2");
  }
  //Serial.println("out of QMTOPEN");
  //delay(500);
  loop();
}

bool QMTCONN()
{
  //Serial.println("inside QMTCONN");
  //delay(10);
  String QMTCONNcheck;

  for (int l = 0; l < 6; l++)
  {
    //Serial.println("QMTCONN loop");
    //delay(500);
    Serial1.print("AT+QMTCONN=0,\"TEST4\",\"hv.9977238645@gmail.com\",\"b2c8fa59\"\r\n");
    //Serial1.print("AT+QMTCONN=0,\"user1\"\r\n");
    
    delay(5000);
    QMTCONNcheck = readSerial();
    //Serial.println(QMTCONNcheck);
    //delay(10);

    /*if ((QMTCONNcheck.indexOf("ER")) == -1)
    {
      //Serial.println("inside QMTCONN if");
      //delay(500);

      return true;
    }*/
    return true;
  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}


bool QMTPUB()
{
  //Serial.println("inside QMTPUB");
  //delay(10);
  String QMTPUBcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    Serial1.print("AT+QMTPUB=0,0,0,0,\"/hv.9977238645@gmail.com/test\"\r");
    //Serial1.print("AT+QMTPUB=0,0,0,0,\"temp/random\"\r");
    delay(10);
    //Serial1.print(Data);
    //delay(500);
    for (int i=0; i<4; i++) {
      Serial1.print(charReceiveByte[i]); 
    }
    //Serial1.print(40);
    Serial1.print("\r\n");
    //Serial1.print("\r\n");
    delay(1000);
    //Serial.println(readSerial());
    Serial1.print(char(26));
    delay(4000);
    QMTPUBcheck = readSerial();
    //Serial.println(QMTPUBcheck);

    /*if ((QMTPUBcheck.indexOf("ER")) == -1)
    {
      //Serial.println("inside no error");
      //delay(500);

      return true;
    }*/
    return true;
  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}


char * sensorRead(String deviceID)
{
  //Serial.println("in sensorRead function");
  //delay(10);

  static char loraByte[4];
  unsigned int i, N, count;
  char ch;
  i = 0; N = 4;
  count = 8000;

  Serial.print(deviceID);

  while (i < N && count > 0)
  {
    if (Serial.available())
    {
      ch = Serial.read();
      loraByte[i] = ch;   
      i++;
      count = 8000;
    }
    else {
      delay(1);
      count--;
    }
  }

  if (i == 4)
  {
    //Serial.println("got 4 characters");
    //do nothing
  }
  else
  {
    loraByte[0] = 'e';
    loraByte[1] = 'e';
    loraByte[2] = 'e';
    loraByte[3] = 'e';

    //Serial.println("lora error sensor value");
    //delay(10);
    //Serial.print( MyByte[i]);
    //Serial.println("MyByte =");
    //Serial.println(MyByte);
    //delay(10);
    //Serial.println("eee,eee,eee,eee,eee");
    //delay(10);
  }

  return loraByte;
}


void lora1Ping()
{
  
  pointerLoraByte = sensorRead("A");

  //Serial.println("received bytes = ");

  for (int i=0; i<4; i++) {

    charReceiveByte[i] = *(pointerLoraByte + i);
    //Serial.print(charReceiveByte[i]);
    //delay(500); 
  }
}


void lora2Ping()
{
  
  pointerLoraByte = sensorRead("B");

  //Serial.println("received bytes = ");

  for (int i=0; i<4; i++) {

    charReceiveByte[i] = *(pointerLoraByte + i);
    //Serial.print(charReceiveByte[i]);
    //delay(500); 
  }
}


HubIIM_v1 (13/02/2018)
-------------------------------------------------------------------------------------------------------------------
# IMPORTANT:

$ Trimmed down version of Hub_v3_1.
-------------------------------------------------------------------------------------------------------------------
# UPDATES:

$ Only timer mode and manual mode enabled in this version.
$ Time is calculated and compared in minutes from this version.
$ Bug with deleteMessage function fixed.
$ Bug with gsmRTC giving zero hours and zero minutes on the first call is fixed.
$ Bug with all AT commands that it gives "CMGS = some number" sometimes is fixed.
$ Bug of sending multiple acknowledgment message for timer and defTimer mode is fixed by
  adding timerMessageCount and deftimerMessageCount.
$ manual mode modified to control individual solenoid valves and pump.
$ Power supply availibilty tester added.
-------------------------------------------------------------------------------------------------------------------
# USAGE MANUAL:

$ "**s" - stops pump and all the three solenoids.
$ "**a" Or "**A" - starts the pump and solenoid valve for amphitheatre.
$ "**m" Or "**M" - starts the pump and solenoid valve for MDC.
$ "**b" Or "**B" - starts the pump and solenoid valve for MDC back.
$ "##timer" - starts the pump and the timer set by the user.
$ "##deftimer" - starts the pump and the timer with default schedule.

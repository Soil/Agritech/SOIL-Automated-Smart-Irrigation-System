#include <Wire.h>

const int pumpon_relay = 40;
const int pumpoff_relay = 39;
const int solenoid1_relay = 38;
const int solenoid2_relay = 37;
const int solenoid3_relay = 36;
const int powerPin = 34;
int flushtime = 5000;
bool AT;
bool textModeSetup;
bool memoryModeSetup;
String allMessagesGrabed;
int timeout;
String buffer;
String manualBuffer;
String timerBuffer;
String defTimerBuffer;
bool messageDeleted;
int powerStatus = 0;
int pumpSwitch;
int pumpStatus;
int sensorMoist;
int modeIndex;
char analogByte[6];
char moistByte[4];
char *pointerCurrentTime;
char *pointerLoraByte;
int globCounter = 0;
int timerMessageCount;
int deftimerMessageCount;
int moistMessageCount;

int powerOnAckSMS;
int powerOffAckSMS;
int AmphiAckSMS;
int mdcAckSMS;
int mdcBackAckSMS;
int moistAmphiAckSMS;
int moistMdcAckSMS;
int moistMdcBackAckSMS;
int waterLowAck;

char charMDCmoistValue[3];
char charMDCbackMoistValue[3];
int moistPriority = 0;

int solenoid1 = 0;
int solenoid2 = 0;
int solenoid3 = 0;
int waterAvail = 1;
char charRTChours[2];
char charRTCminutes[2];

float ultraSensValue;

void setup()
{
  Serial.begin(9600);
  Serial1.begin(9600);

  //Wire.begin();

  pinMode(pumpon_relay, OUTPUT);
  pinMode(pumpoff_relay, OUTPUT);
  pinMode(solenoid1_relay, OUTPUT);
  pinMode(solenoid2_relay, OUTPUT);
  pinMode(solenoid3_relay, OUTPUT);
  pinMode(powerPin, INPUT_PULLUP);

  digitalWrite(pumpon_relay, LOW);
  digitalWrite(pumpoff_relay, LOW);
  digitalWrite(solenoid1_relay, LOW);
  digitalWrite(solenoid2_relay, LOW);
  digitalWrite(solenoid3_relay, LOW);

  powerOnAckSMS = 0;
  powerOffAckSMS = 0;
  AmphiAckSMS = 0;
  mdcAckSMS = 0;
  mdcBackAckSMS = 0;
  timerMessageCount = 0;
  deftimerMessageCount = 0;

  buffer.reserve(255);
}


void loop()
{
  //Serial.println("test serial");
  powerStatus = digitalRead(powerPin);

  delay(100);
  //Serial.println("test serial port");

  if (powerStatus == 0) //powerStatus == 0, when power supply is available
  {
    //Serial.println("power supply available");
    if (powerOnAckSMS = 0)
    {
      smsAckSend("power supply available", "+918349504911");
      powerOnAckSMS++;
    }

    powerOffAckSMS = 0;

    //Serial.println("calling ATinitiation function");
    //delay(10);
    AT = ATinitiation();
    delay(100);

    //Serial.println("calling setTextmode function");
    //delay(10);
    textModeSetup = setTextmode();
    delay(100);

    //Serial.println("calling setMemoryType function");
    //delay(10);
    memoryModeSetup = setMemoryType();
    delay(100);

    //Serial.println("calling grabAllMessages function");
    //delay(10);
    allMessagesGrabed = grabAllMessages();
    delay(100);

    //Serial.println("calling modeSelector funct from main loop");
    //delay(10);
    modeSelector();
    delay(100);
    /*
    if (modeIndex != 4) {
      Serial.println("modeIndex != 4");
      mdcMoistSensor();
      mdcBackMoistSensor();

      pointerCurrentTime = gsmRTC();

      sdWrite('O');
    }

    ultrasonicSens();
    */
  }

  if (powerStatus == 1) //powerStatus == 1, when power supply is not available
  {
    //Serial.println("no power supply");
    if (powerOffAckSMS = 0)
    {
      smsAckSend("no power supply", "+918349504911");
      powerOffAckSMS++;
    }

    powerOnAckSMS = 0;

  }
}


String readSerial()
{
  timeout = 0;

  while  (!Serial1.available() && timeout < 12000  )
  {
    delay(13);
    timeout++;
  }


  if (Serial1.available())
  {
    return Serial1.readString();
  }
}


void flushReceive(long time)
{
  digitalWrite(GREEN_LED, HIGH);
  int time_start = millis();
  while (Serial1.available() > 0 || (millis() - time_start < time))
  {
    delay(10);
    if (Serial1.available() > 0)
    {
      char c = Serial1.read();
      if (c == '\r' || c == '\n')
      {
        continue;
      }
    }
  }
  digitalWrite(GREEN_LED, LOW);
}


bool ATinitiation()
{
  //Serial.println("inside ATinitiation");
  //delay(10);
  String ATcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    Serial1.print("AT\r");
    delay(500);
    ATcheck = readSerial();
    //Serial.println(ATcheck);
    //delay(10);

    if ((ATcheck.indexOf("ER")) == -1)
    {
      if ((ATcheck.indexOf("AT")) != -1)
      {
        //Serial.println("no ERR in ATinitiation");
        //delay(10);

        return true;
      }
    }

  }
  //Serial.println("out of AT");
  //delay(10);
  loop();
}


bool setTextmode()
{
  //Serial.println("in setTextmode function");
  //delay(10);

  String setTextModeCheck;

  for (int k = 0; k < 6; k++)
  {
    Serial1.print("AT + CMGF = 1\r");
    delay(500);
    setTextModeCheck = readSerial();
    //Serial.println(setTextModeCheck);
    //delay(10);
    if ((setTextModeCheck.indexOf("ER")) == -1)
    {
      if ((setTextModeCheck.indexOf("AT")) != -1)
      {
        //Serial.println("no ERR in setTextmode");
        //delay(10);
        return true;
      }
    }

  }
  //return false;
  //Serial.println("error: out of setTextmode loop\r\n");
  //delay(500);
  loop();
}


bool setMemoryType()
{
  //Serial.println("in setMemoryType function");
  //delay(10);

  for (int i = 0; i < 6; i++)
  {
    Serial1.print("AT + CPMS = \"SM\"\r");
    delay(500);
    String memType = readSerial();
    //Serial.println(memType);
    //delay(10);
    if ((memType.indexOf("ER")) == -1)
    {
      if ((memType.indexOf("AT")) != -1)
      {
        //Serial.println("no ERR in setMemoryType function");
        //delay(10);
        return true;
      }
    }
    //Serial.println("inside setMemoryType out of loop\r\n");
  }
  //return false;
  //Serial.println("error: out of setMemoryType loop");
  //delay(500);
  loop();
}


String grabAllMessages()
{
  //Serial.println("in grabAllMessages function");
  //delay(10);

  for (int j = 0; j < 6; j++)
  {
    //delay(3000);
    flushReceive(flushtime);
    delay(1000);
    Serial1.print("AT + CMGR= 1\r");
    delay(4000);
    buffer = readSerial();
    //delay(2000);
    //Serial.println("buffer =");
    //delay(10);
    //Serial.println(buffer);
    //delay(10);

    if ((buffer.indexOf("ER")) == -1)
    {
      //Serial.println("grabAllMessages no error");
      //delay(10);

      if (buffer.indexOf("+CMGR:") != -1)
      {

        //Serial.println("grabAllMessages +CMGR found");
        //delay(10);

        if (buffer.indexOf("**s") != -1)
        {
          //Serial.println("found stop");
          //delay(10);

          timerMessageCount = 0; // To avoid multiple message sending for other operating modes
          deftimerMessageCount = 0; // To avoid multiple message sending for other operating modes

          modeIndex = 0;

          //Serial.println("modeIndex =");
          //delay(10);
          //Serial.println(modeIndex);
          //delay(10);

          //Serial.println("calling modeSelector function & modeSelector=0");
          //delay(10);
          modeSelector();

          //Serial.println("again inside grabAllMessages calling deleteMessage function");
          //delay(10);
          deleteMessage();

          delay(10);
          return buffer;
        }


        if (buffer.indexOf("**") != -1)
        {
          //Serial.println("found manual mode");
          //delay(10);

          timerMessageCount = 0; // To avoid multiple message sending for other operating modes
          deftimerMessageCount = 0; // To avoid multiple message sending for other operating modes
          moistMessageCount = 0;

          modeIndex = 1;
          //Serial.println("modeIndex =");
          //delay(10);
          //Serial.println(modeIndex);
          //delay(10);

          manualBuffer = buffer;
          delay(100);
          //Serial.println("manualBuffer =");
          //delay(10);
          //Serial.println(manualBuffer);
          //delay(100);

          //parsePhoneNumber(buffer);
          //Serial.println("calling modeSelector function & modeSelector=1");
          //delay(10);
          modeSelector();

          //Serial.println("again inside grabAllMessages calling deleteMessage function");
          //delay(10);
          deleteMessage();

          delay(10);
          return buffer;
        }



        if (buffer.indexOf("##timer") != -1)
        {
          //Serial.println("found timer mode");
          //delay(10);

          deftimerMessageCount = 0; // To avoid multiple message sending for other operating modes
          moistMessageCount = 0;

          modeIndex = 2;
          //Serial.println("modeIndex =");
          //delay(10);
          //Serial.println(modeIndex);
          //delay(10);
          timerBuffer = buffer;
          delay(100);
          //Serial.println("timerBuffer =");
          //delay(10);
          //Serial.println(timerBuffer);
          //delay(100);

          if (timerMessageCount == 0)
          {
            String smsTimerMessage = "Timer mode ON";
            smsAckSend(smsTimerMessage, timerBuffer);
            delay(10);
            timerMessageCount++;
          }

          //Serial.println("calling modeSelector function & modeSelector=2");
          //delay(10);
          modeSelector();

          //Serial.println("again inside grabAllMessages calling deleteMessage function");
          //delay(10);
          deleteMessage();

          delay(10);
          return buffer;
        }

        if (buffer.indexOf("##deftimer") != -1)
        {
          //Serial.println("found default timer mode");
          //delay(10);

          timerMessageCount = 0; //To avoid multiple message sending for other operating modes
          moistMessageCount = 0;

          modeIndex = 3;
          //Serial.println("modeIndex =");
          //delay(10);
          //Serial.println(modeIndex);
          //delay(10);
          defTimerBuffer = buffer;
          delay(100);
          //Serial.println("defTimerBuffer =");
          //delay(10);
          //Serial.println(defTimerBuffer);
          //delay(100);

          if (deftimerMessageCount == 0)
          {
            String smsTimerMessage = "default Timer mode ON";
            smsAckSend(smsTimerMessage, defTimerBuffer);
            delay(10);
            deftimerMessageCount++;
          }

          //Serial.println("calling modeSelector function & modeSelector = 3");
          //delay(10);
          modeSelector();

          //Serial.println("again inside grabAllMessages calling deleteMessage function");
          //delay(10);
          deleteMessage();

          delay(10);
          return buffer;
        }

        if (buffer.indexOf("##moist") != -1)
        {
          //Serial.println("found oisture ode");
          //delay(10);

          timerMessageCount = 0;
          deftimerMessageCount = 0; // To avoid multiple message sending for other operating modes

          modeIndex = 4;

          if (moistMessageCount == 0)
          {
            String smsMoistMessage = "moisture mode ON";
            smsAckSend(smsMoistMessage, "+918349504911");
            delay(10);
            moistMessageCount++;
          }

          //Serial.println("calling modeSelector function & modeSelector=4");
          //delay(10);
          modeSelector();

          //Serial.println("again inside grabAllMessages calling deleteMessage function");
          //delay(10);
          deleteMessage();

          delay(10);
          return buffer;
        }


        else
        {
          //Serial.println("not found any message");
          //delay(500);
          //Serial.println("no identifier in message so calling deleteMessage");
          //delay(10);
          deleteMessage();
          return buffer;
        }

      }

    }
    else
    {
      //Serial.println("no message received so calling deleteMessage for any garbage value");
      //delay(10);
      deleteMessage();
    }
    return buffer;
  }
  //return "error: unable to run AT+CMGR\r\n";
  //Serial.println("error: out of grabAllMessages loop");
  //delay(500);
  //loop();
  return buffer;
}


void modeSelector()
{
  switch (modeIndex)
  {
    case 0:
      //delay(10);
      //Serial.println("In modeIndex = 0");
      //delay(10);
      //Serial.println("calling manualModePumpStop function");
      //delay(10);
      manualModePumpStop();
      delay(100);
      break;

    case 1:
      //delay(10);
      //Serial.println("In modeIndex = 1");
      //delay(10);
      //Serial.println("calling manualModePumpStart function");
      //delay(10);
      manualModePumpStart();
      delay(100);
      break;

    case 2:
      //delay(10);
      //Serial.println("In modeIndex = 2");
      //delay(10);
      //Serial.println("calling timerMode function");
      //delay(10);
      timerMode();
      delay(100);
      break;

    case 3:
      //delay(10);
      //Serial.println("In modeIndex = 3");
      //delay(10);
      //Serial.println("calling defTimerMode function");
      //delay(10);
      defTimerMode();
      delay(100);
      break;

    case 4:
      //delay(10);
      //Serial.println("In odeIndex = 4");
      //delay(10);
      //Serial.println("calling moistureMode function");
      //delay(10);
      moistureMode();
      delay(100);
      break;

  }
}


void manualModePumpStart()
{

  //Serial.println("in manualModePumpStart");
  //delay(10);

  if (manualBuffer.indexOf("**A") != -1 || manualBuffer.indexOf("**a") != -1)
  {
    //Serial.println("Amphitheatre relay ON");
    //delay(10);
    digitalWrite(solenoid1_relay, HIGH);
    digitalWrite(solenoid2_relay, LOW);
    digitalWrite(solenoid3_relay, LOW);

    solenoid1 = 1;
    solenoid2 = 0;
    solenoid3 = 0;

    AmphiAckSMS++;
    mdcAckSMS = 0;
    mdcBackAckSMS = 0;
    //Serial.println("AmphiAckSMS = ");
    //delay(10);
    //Serial.println(AmphiAckSMS);
    //delay(10);
  }

  if (manualBuffer.indexOf("**M") != -1 || manualBuffer.indexOf("**m") != -1)
  {
    //Serial.println("MDC relay ON");
    //delay(10);
    digitalWrite(solenoid1_relay, LOW);
    digitalWrite(solenoid2_relay, HIGH);
    digitalWrite(solenoid3_relay, LOW);

    solenoid1 = 0;
    solenoid2 = 1;
    solenoid3 = 0;

    AmphiAckSMS = 0;
    mdcAckSMS++;
    mdcBackAckSMS = 0;
    //Serial.println("mdcAckSMS = ");
    //delay(10);
    //Serial.println(mdcAckSMS);
    //delay(10);
  }

  if (manualBuffer.indexOf("**B") != -1 || manualBuffer.indexOf("**b") != -1)
  {
    //Serial.println("MDC back relay ON");
    //delay(10);
    digitalWrite(solenoid1_relay, LOW);
    digitalWrite(solenoid2_relay, LOW);
    digitalWrite(solenoid3_relay, HIGH);

    solenoid1 = 0;
    solenoid2 = 0;
    solenoid3 = 1;

    AmphiAckSMS = 0;
    mdcAckSMS = 0;
    mdcBackAckSMS++;
    //Serial.println("mdcBackAckSMS = ");
    //delay(10);
    //Serial.println(mdcBackAckSMS);
    //delay(10);
  }

  if (AmphiAckSMS == 1)
  {
    //Serial.println("sending AmphiAckSMS");
    //delay(10);
    smsAckSend("Amphitheatre ON", "+918349504911");
  }

  if (mdcAckSMS == 1)
  {
    //Serial.println("sending mdcAckSMS");
    //delay(10);
    smsAckSend("MDC relay ON", "+918349504911");
  }

  if (mdcBackAckSMS == 1)
  {
    //Serial.println("sending mdcBackAckSMS");
    //delay(10);
    smsAckSend("MDC back relay ON", "+918349504911");
  }

  pumpSwitch = 1;
  //Serial.println("calling pump function");
  //delay(10);
  pump();
}


void manualModePumpStop()
{
  //Serial.println("in manualModePumpStop");
  //delay(10);
  pumpSwitch = 0;
  //Serial.println("calling pump function");
  //delay(10);
  pump();

  digitalWrite(solenoid1_relay, LOW);
  digitalWrite(solenoid2_relay, LOW);
  digitalWrite(solenoid3_relay, LOW);
}


void timerMode()
{
  String pattern = "#";
  int patternIndex;
  char charBuffer[150];
  char charpattern[20];
  char message[9];
  int startHours;
  int startMinutes;
  int stopHours;
  int stopMinutes;
  //int intMessage;
  int *pointerTimeDigits;
  int timeDigits[9];

  //Serial.println("in timer mode");
  //delay(10);
  //Serial.println(line[8]);
  //delay(500);
  timerBuffer.toCharArray(charBuffer, 150);
  delay(500);
  pattern.toCharArray(charpattern, 20);
  delay(500);
  //Serial.println("charBuffer =");
  //delay(50);
  //Serial.println(charBuffer);
  //delay(100);

  int h, l = 0;
  patternIndex = stringIndexer(charBuffer, charpattern);
  //Serial.println("patternIndex =");
  //delay(50);
  //Serial.println(patternIndex);
  for (h = patternIndex + 1; l < 9; h++, l++)
  {
    //Serial.println(l);
    //Serial.println(h);
    //Serial.println("inside loop");
    message[l] = charBuffer[h];
  }
  //Serial.println("message =");
  //delay(50);
  //Serial.println(message);
  //delay(100);
  pointerTimeDigits = messageFormatter(message, l - 2);

  for (int i = 0; i < l; i++)
  {
    timeDigits[i] = *(pointerTimeDigits + i);
    //Serial.println("i =");
    //delay(10);
    //Serial.println(i);
    //delay(10);
    //Serial.println("timeDigits");
    //delay(10);
    //Serial.println(timeDigits[i]);
  }

  //Serial.println("timeDigits =");
  //delay(10);
  //Serial.println(*timeDigits);
  //delay(10);

  startHours = ((timeDigits[0] * 10) + timeDigits[1]);
  //Serial.println("startHours =");
  //delay(10);
  //Serial.println(startHours);
  //delay(10);

  startMinutes = ((timeDigits[2] * 10) + timeDigits[3]);
  //Serial.println("startMinutes =");
  //delay(10);
  //Serial.println(startMinutes);
  //delay(100);

  stopHours = ((timeDigits[4] * 10) + timeDigits[5]);
  //Serial.println("stopHours =");
  //delay(10);
  //Serial.println(stopHours);
  //delay(100);

  stopMinutes = ((timeDigits[6] * 10) + timeDigits[7]);
  //Serial.println("stopMinutes =");
  //delay(10);
  //Serial.println(stopMinutes);
  //delay(100);

  //Serial.println("calling minutesConverter for start and stop time");
  //delay(10);
  int startTime = minutesConverter(startHours, startMinutes);
  int stopTime = minutesConverter(stopHours, stopMinutes);

  //Serial.println("startTime =");
  //delay(10);
  //Serial.println(startTime);
  //delay(10);
  //Serial.println("stopTime =");
  //delay(10);
  //Serial.println(stopTime);
  //delay(10);

  //Serial.println("calling gsmRTC");
  //delay(10);
  pointerCurrentTime = gsmRTC();

  //Serial.println("calling RTChours");
  //delay(10);
  int realHours = RTChours();
  //Serial.println("realHours =");
  //delay(10);
  //Serial.println(realHours);
  //delay(10);

  //Serial.println("calling RTCminutes");
  //delay(10);
  int realminutes = RTCminutes();
  //Serial.println("realminutes =");
  //delay(10);
  //Serial.println(realminutes);
  //delay(10);

  //Serial.println("calling minutesConverter for realTime");
  //delay(10);
  int realTime = minutesConverter(realHours, realminutes);

  //Serial.println("realTime and timer from message fetched");
  //delay(10);
  //Serial.println("calling timerComparision function");
  //delay(10);
  timerComparision(startTime, stopTime, realTime, 1, 1, 1);
}


void defTimerMode()
{
  //Serial.println("in defTimerMode");
  //delay(10);
  int solenoid1;
  int solenoid2;
  int solenoid3;

  //Serial.println("calling gsmRTC");
  //delay(10);
  pointerCurrentTime = gsmRTC();

  //Serial.println("calling RTChours");
  //delay(10);
  int realHours = RTChours();
  //Serial.println("realHours =");
  //delay(10);
  //Serial.println(realHours);
  //delay(10);

  //Serial.println("calling RTCminutes");
  //delay(10);
  int realminutes = RTCminutes();
  //Serial.println("realminutes =");
  //delay(10);
  //Serial.println(realminutes);
  //delay(10);

  //Serial.println("calling minutesConverter for realTime");
  //delay(10);
  int realTime = minutesConverter(realHours, realminutes);
  //Serial.println("realTime =");
  //delay(10);
  //Serial.println(realTime);
  //delay(10);


  int startHour1 = 8; int startMin1 = 00; int stopHour1 = 8; int stopMin1 = 30;
  int startHour2 = 8; int startMin2 = 30; int stopHour2 = 9; int stopMin2 = 00;
  int startHour3 = 9; int startMin3 = 00; int stopHour3 = 9; int stopMin3 = 30;
  int startHour4 = 9; int startMin4 = 30; int stopHour4 = 10; int stopMin4 = 00;
  int startHour5 = 10; int startMin5 = 00; int stopHour5 = 10; int stopMin5 = 30;
  int startHour6 = 10; int startMin6 = 30; int stopHour6 = 11; int stopMin6 = 00;
  int startHour7 = 11; int startMin7 = 00; int stopHour7 = 11; int stopMin7 = 30;
  int startHour8 = 11; int startMin8 = 30; int stopHour8 = 12; int stopMin8 = 00;
  int startHour9 = 12; int startMin9 = 00; int stopHour9 = 12; int stopMin9 = 30;
  int startHour10 = 12; int startMin10 = 30; int stopHour10 = 13; int stopMin10 = 00;
  int startHour11 = 13; int startMin11 = 00; int stopHour11 = 13; int stopMin11 = 30;
  int startHour12 = 13; int startMin12 = 30; int stopHour12 = 14; int stopMin12 = 00;
  int startHour13 = 14; int startMin13 = 00; int stopHour13 = 14; int stopMin13 = 30;
  int startHour14 = 14; int startMin14 = 30; int stopHour14 = 15; int stopMin14 = 00;
  int startHour15 = 15; int startMin15 = 00; int stopHour15 = 15; int stopMin15 = 30;
  int startHour16 = 15; int startMin16 = 30; int stopHour16 = 16; int stopMin16 = 00;

  int startTime1 = minutesConverter(startHour1, startMin1); int stopTime1 = minutesConverter(stopHour1, stopMin1);
  int startTime2 = minutesConverter(startHour2, startMin2); int stopTime2 = minutesConverter(stopHour2, stopMin2);
  int startTime3 = minutesConverter(startHour3, startMin3); int stopTime3 = minutesConverter(stopHour3, stopMin3);
  int startTime4 = minutesConverter(startHour4, startMin4); int stopTime4 = minutesConverter(stopHour4, stopMin4);
  int startTime5 = minutesConverter(startHour5, startMin5); int stopTime5 = minutesConverter(stopHour5, stopMin5);
  int startTime6 = minutesConverter(startHour6, startMin6); int stopTime6 = minutesConverter(stopHour6, stopMin6);
  int startTime7 = minutesConverter(startHour7, startMin7); int stopTime7 = minutesConverter(stopHour7, stopMin7);
  int startTime8 = minutesConverter(startHour8, startMin8); int stopTime8 = minutesConverter(stopHour8, stopMin8);
  int startTime9 = minutesConverter(startHour9, startMin9); int stopTime9 = minutesConverter(stopHour9, stopMin9);
  int startTime10 = minutesConverter(startHour10, startMin10); int stopTime10 = minutesConverter(stopHour10, stopMin10);
  int startTime11 = minutesConverter(startHour11, startMin11); int stopTime11 = minutesConverter(stopHour11, stopMin11);
  int startTime12 = minutesConverter(startHour12, startMin12); int stopTime12 = minutesConverter(stopHour12, stopMin12);
  int startTime13 = minutesConverter(startHour13, startMin13); int stopTime13 = minutesConverter(stopHour13, stopMin13);
  int startTime14 = minutesConverter(startHour14, startMin14); int stopTime14 = minutesConverter(stopHour14, stopMin14);
  int startTime15 = minutesConverter(startHour15, startMin15); int stopTime15 = minutesConverter(stopHour15, stopMin15);
  int startTime16 = minutesConverter(startHour16, startMin16); int stopTime16 = minutesConverter(stopHour16, stopMin16);

  if (realTime >= startTime1 && realTime <= stopTime1)
  {
    //Serial.println("timer from 08:00 to 08:30");
    //delay(10);
    //Serial.println("solenoid1 ON");
    solenoid1 = 1;
    solenoid2 = 0;
    solenoid3 = 0;
    timerComparision(startTime1, stopTime1, realTime, solenoid1, solenoid2, solenoid3);
    return;
  }

  if (realTime >= startTime2 && realTime <= stopTime2)
  {
    //Serial.println("timer from 08:30 to 09:00");
    //delay(10);
    //Serial.println("solenoid1 ON");
    solenoid1 = 0;
    solenoid2 = 1;
    solenoid3 = 0;
    timerComparision(startTime2, stopTime2, realTime, solenoid1, solenoid2, solenoid3);
    return;
  }

  if (realTime >= startTime3 && realTime <= stopTime3)
  {
    //Serial.println("timer from 09:00 to 09:30");
    //delay(10);
    //Serial.println("solenoid1 ON");
    solenoid1 = 0;
    solenoid2 = 0;
    solenoid3 = 1;
    timerComparision(startTime3, stopTime3, realTime, solenoid1, solenoid2, solenoid3);
    return;
  }

  if (realTime >= startTime4 && realTime <= stopTime4)
  {
    //Serial.println("timer from 09:30 to 10:00");
    //delay(10);
    //Serial.println("solenoid1 ON");
    solenoid1 = 1;
    solenoid2 = 0;
    solenoid3 = 0;
    timerComparision(startTime4, stopTime4, realTime, solenoid1, solenoid2, solenoid3);
    return;
  }

  if (realTime >= startTime5 && realTime <= stopTime5)
  {
    //Serial.println("timer from 10:00 to 10:30");
    //delay(10);
    //Serial.println("solenoid1 ON");
    solenoid1 = 0;
    solenoid2 = 1;
    solenoid3 = 0;
    timerComparision(startTime5, stopTime5, realTime, solenoid1, solenoid2, solenoid3);
    return;
  }

  if (realTime >= startTime6 && realTime <= stopTime6)
  {
    //Serial.println("timer from 10:30 to 11:00");
    //delay(10);
    //Serial.println("solenoid1 ON");
    solenoid1 = 0;
    solenoid2 = 0;
    solenoid3 = 1;
    timerComparision(startTime6, stopTime6, realTime, solenoid1, solenoid2, solenoid3);
    return;
  }

  if (realTime >= startTime7 && realTime <= stopTime7)
  {
    //Serial.println("timer from 11:00 to 11:30");
    //delay(10);
    //Serial.println("solenoid1 ON");
    solenoid1 = 1;
    solenoid2 = 0;
    solenoid3 = 0;
    timerComparision(startTime7, stopTime7, realTime, solenoid1, solenoid2, solenoid3);
    return;
  }

  if (realTime >= startTime8 && realTime <= stopTime8)
  {
    //Serial.println("timer from 11:30 to 12:00");
    //delay(10);
    //Serial.println("solenoid2 ON");
    solenoid1 = 0;
    solenoid2 = 1;
    solenoid3 = 0;
    timerComparision(startTime8, stopTime8, realTime, solenoid1, solenoid2, solenoid3);
    return;
  }

  if (realTime >= startTime9 && realTime <= stopTime9)
  {
    //Serial.println("timer from 12:00 to 12:30");
    //delay(10);
    //Serial.println("solenoid3 ON");
    solenoid1 = 0;
    solenoid2 = 0;
    solenoid3 = 1;
    timerComparision(startTime9, stopTime9, realTime, solenoid1, solenoid2, solenoid3);
    return;
  }

  if (realTime >= startTime10 && realTime <= stopTime10)
  {
    //Serial.println("timer from 12:30 to 13:00");
    //delay(10);
    //Serial.println("solenoid1 ON");
    solenoid1 = 1;
    solenoid2 = 0;
    solenoid3 = 0;
    timerComparision(startTime10, stopTime10, realTime, solenoid1, solenoid2, solenoid3);
    return;
  }

  if (realTime >= startTime11 && realTime <= stopTime11)
  {
    //Serial.println("timer from 13:00 to 13:30");
    //delay(10);
    //Serial.println("solenoid2 ON");
    solenoid1 = 0;
    solenoid2 = 1;
    solenoid3 = 0;
    timerComparision(startTime11, stopTime11, realTime, solenoid1, solenoid2, solenoid3);
    return;
  }

  if (realTime >= startTime12 && realTime <= stopTime12)
  {
    //Serial.println("timer from 13:30 to 14:00");
    //delay(10);
    //Serial.println("solenoid3 ON");
    solenoid1 = 0;
    solenoid2 = 0;
    solenoid3 = 1;
    timerComparision(startTime12, stopTime12, realTime, solenoid1, solenoid2, solenoid3);
    return;
  }

  if (realTime >= startTime13 && realTime <= stopTime13)
  {
    //Serial.println("timer from 14:00 to 14:30");
    //delay(10);
    //Serial.println("solenoid1 ON");
    solenoid1 = 1;
    solenoid2 = 0;
    solenoid3 = 0;
    timerComparision(startTime13, stopTime13, realTime, solenoid1, solenoid2, solenoid3);
    return;
  }

  if (realTime >= startTime14 && realTime <= stopTime14)
  {
    //Serial.println("timer from 14:30 to 15:00");
    //delay(10);
    //Serial.println("solenoid2 ON");
    solenoid1 = 0;
    solenoid2 = 1;
    solenoid3 = 0;
    timerComparision(startTime14, stopTime14, realTime, solenoid1, solenoid2, solenoid3);
    return;
  }

  if (realTime >= startTime15 && realTime <= stopTime15)
  {
    //Serial.println("timer from 15:00 to 15:30");
    //delay(10);
    //Serial.println("solenoid3 ON");
    solenoid1 = 0;
    solenoid2 = 0;
    solenoid3 = 1;
    timerComparision(startTime15, stopTime15, realTime, solenoid1, solenoid2, solenoid3);
    return;
  }

  if (realTime >= startTime16 && realTime <= stopTime16)
  {
    //Serial.println("timer from 15:30 to 16:00");
    //delay(10);
    //Serial.println("solenoid1 ON");
    solenoid1 = 1;
    solenoid2 = 0;
    solenoid3 = 0;
    timerComparision(startTime16, stopTime16, realTime, solenoid1, solenoid2, solenoid3);
    return;
  }
}


void moistureMode()
{
  //Serial.println("in oistureode function");
  //delay(10);

  int MDCmoistureValue;
  int MDCbackMoistureValue;
  int moistHighThreshold = 80;
  int moistLowThreshold = 50;


  //Serial.println("calling mdcMoistSensor function");
  //delay(10);
  MDCmoistureValue = mdcMoistSensor();

  //Serial.println("calling mdcMoistSensor function");
  //delay(10);
  MDCbackMoistureValue = mdcBackMoistSensor();

  //Serial.println("calling gsmRTC function");
  //delay(10);
  //pointerCurrentTime = gsmRTC();

  //Serial.println("calling sdWrite function");
  //delay(10);
  //sdWrite('M');

  //Serial.println("calling sdWrite function");
  //delay(10);
  //sdWrite('B');
  /*
    if (charMDCmoistValue[0] != 'e') {

    //Serial.println("no error in charMDCmoistValue");
    //delay(10);

    if ((MDCmoistureValue < moistLowThreshold) && moistPriority == 0) {

      digitalWrite(solenoid1_relay, LOW);
      digitalWrite(solenoid2_relay, HIGH);
      digitalWrite(solenoid3_relay, LOW);

      solenoid1 = 0;
      solenoid2 = 1;
      solenoid3 = 0;

      moistPriority = 1;

      pumpSwitch = 1;
      pump();

      smsAckSend("moistMDC relay ON", "+918349504911");
    }

    if (MDCmoistureValue >= moistHighThreshold) {

      pumpSwitch = 0;
      pump();

      digitalWrite(solenoid2_relay, LOW);
      moistPriority = 0;
    }
    }

    if (charMDCbackMoistValue[0] != 'e') {

    //Serial.println("no error in charMDCbackMoistValue");
    //delay(10);

    if (MDCbackMoistureValue < moistLowThreshold && moistPriority == 0) {

      digitalWrite(solenoid1_relay, LOW);
      digitalWrite(solenoid2_relay, LOW);
      digitalWrite(solenoid3_relay, HIGH);

      solenoid1 = 0;
      solenoid2 = 0;
      solenoid3 = 1;

      moistPriority = 1;

      pumpSwitch = 1;
      pump();

      smsAckSend("moistMDCback relay ON", "+918349504911");
    }

    if (MDCbackMoistureValue >= moistHighThreshold) {

      pumpSwitch = 1;
      pump();

      digitalWrite(solenoid3_relay, LOW);
      moistPriority = 0;
    }
    }

    else {

    //Serial.println("error in sensor values");
    //delay(10);
    }
  */
}


int mdcMoistSensor()
{
  //Serial.println("in dcoistSensor function");
  //delay(10);

  int intMDCmoistValue;

  pointerLoraByte = sensorRead("M");


  int x = 0;
  for (int y = 0; y < 3; x++, y++)
  {
    charMDCmoistValue[x] = *(pointerLoraByte + y);
  }

  intMDCmoistValue = atol(charMDCmoistValue);

  //Serial.println("intDCmoistValue =");
  //delay(10);
  //Serial.println(intMDCmoistValue);
  //delay(10);

  return intMDCmoistValue;
}


int mdcBackMoistSensor()
{
  //Serial.println("in dcBackoistSensor function");
  //delay(10);

  int intMDCbackMoistValue;

  pointerLoraByte = sensorRead("B");

  int x = 0;
  for (int y = 0; y < 3; x++, y++)
  {
    charMDCbackMoistValue[x] = *(pointerLoraByte + y);
  }

  intMDCbackMoistValue = atol(charMDCbackMoistValue);

  //Serial.println("intDCbackoistValue =");
  //delay(10);
  //Serial.println(intMDCbackMoistValue);
  //delay(10);

  return intMDCbackMoistValue;
}
/*
   returns the RTC year from network.
   IMPORTANT: first call the gsmRTC fuction once and save it to pointerCurrentTime variable before calling
   RTCyear/RTCmonth/RTCdate/RTChours/RTCminutes fuctions. .
*/
int RTCyear()
{
  //Serial.println("in RTCyear");
  //delay(10);

  char charRTCyear[2];
  int intRTCyear;
  int x = 0;

  for (int y = 0; y < 2; x++, y++)
  {
    charRTCyear[x] = *(pointerCurrentTime + y);
  }

  intRTCyear = atol(charRTCyear);

  //Serial.println("intRTCyear =");
  //delay(10);
  //Serial.println(intRTCyear);
  //delay(10);

  return intRTCyear;
}


/*
   returns the RTC month from network.
   IMPORTANT: first call the gsmRTC fuction once and save it to pointerCurrentTime variable before calling
   RTCyear/RTCmonth/RTCdate/RTChours/RTCminutes fuctions. .
*/
int RTCmonth()
{
  //Serial.println("in RTCmonth");
  //delay(10);

  char charRTCmonth[2];
  int intRTCmonth;
  int x = 0;

  for (int y = 3; y < 5; x++, y++)
  {
    charRTCmonth[x] = *(pointerCurrentTime + y);
  }

  intRTCmonth = atol(charRTCmonth);

  //Serial.println("intRTCmonth =");
  //delay(10);
  //Serial.println(intRTCmonth);
  //delay(10);

  return intRTCmonth;
}


/*
   returns the RTC date from network.
   IMPORTANT: first call the gsmRTC fuction once and save it to pointerCurrentTime variable before calling
   RTCyear/RTCmonth/RTCdate/RTChours/RTCminutes fuctions. .
*/
int RTCdate()
{
  //Serial.println("in RTCdate");
  //delay(10);

  char charRTCdate[2];
  int intRTCdate;
  int x = 0;

  for (int y = 6; y < 8; x++, y++)
  {
    charRTCdate[x] = *(pointerCurrentTime + y);
  }

  intRTCdate = atol(charRTCdate);

  //Serial.println("intRTCdate =");
  //delay(10);
  //Serial.println(intRTCdate);
  //delay(10);

  return intRTCdate;
}


/*
   returns the RTC hours from network.
   IMPORTANT: first call the gsmRTC fuction once and save it to pointerCurrentTime variable before calling
   RTCyear/RTCmonth/RTCdate/RTChours/RTCminutes fuctions. .
*/
int RTChours()
{
  //Serial.println("in RTChours");
  //delay(10);


  int intRTChours;
  int x = 0;

  for (int y = 9; y < 11; x++, y++)
  {
    charRTChours[x] = *(pointerCurrentTime + y);
  }

  intRTChours = atol(charRTChours);

  //Serial.println("intRTChours =");
  //delay(10);
  //Serial.println(intRTChours);
  //delay(10);

  return intRTChours;
}


/*
   returns the RTC minutes from network.
   IMPORTANT: first call the gsmRTC fuction once and save it to pointerCurrentTime variable before calling
   RTCyear/RTCmonth/RTCdate/RTChours/RTCminutes fuctions. .
*/
int RTCminutes()
{
  int intRTCminutes;
  int x = 0;

  for (int y = 12; y < 14; x++, y++)
  {
    charRTCminutes[x] = *(pointerCurrentTime + y);
  }

  intRTCminutes = atol(charRTCminutes);

  //Serial.println("intRTCminutes =");
  //delay(10);
  //Serial.println(intRTCminutes);
  //delay(10);

  return intRTCminutes;
}


//--------finds a pattern in a character array and returns its index---------//
int stringIndexer(char source[], char sign[])
{
  int i, j, k, result;

  result = -1;

  for (i = 0; source[i] != '\0'; i++)
  {
    for (j = i, k = 0; sign[k] != '\0' && source[j] == sign[k]; j++, k++)
      ;
    //Serial.println(k);
    //delay(100);
    if (k > 0 && sign[k] == '\0')
    {
      //Serial.println("inside if");
      //delay(100);
      result = i;
      //Serial.println("interimResult =" + result );
    }
  }
  return result;
}


//---------converts character array to integer and save it in a particular format---------//
int * messageFormatter(char charMessage[], int maxIndex)
{
  unsigned int intMessage;
  static int dig[12];
  //String strMessage;

  //strMessage = String(charMessage);
  intMessage = atol(charMessage);

  int j = maxIndex;
  //int j = 7;

  for (int i = j; i > -1 ; i--)
  {
    dig[i] = intMessage % 10;
    //Serial.println("j =");
    //delay(10);
    //Serial.println(i);
    //delay(10);
    //Serial.println("dig =");
    //delay(10);
    //Serial.println(dig[i]);
    //delay(10);
    intMessage = intMessage / 10;
  }
  return dig;
}


//---------converts 24 hours format to minutes----------//
int minutesConverter(int hours, int minutes)
{
  int totalMin = ((hours) * 60) + minutes;
  return totalMin;
}


//---------fetches real time from the network---------//
char * gsmRTC()
{
  //Serial.println("in gsRTC");
  //delay(10);

  String cclkOut;
  static char currentTime[14];

  for (int i = 0; i < 6; i++)
  {

    Serial1.print("AT+CCLK?\r");
    delay(500);
    cclkOut = readSerial();
    //delay(500);
    //Serial.println(cclkOut);
    //delay(10);


    if (cclkOut.indexOf("ER") == -1)
    {
      if ((cclkOut.indexOf("AT")) != -1)
      {
        //Serial.println("no ERR in gsmRTC");
        //delay(10);

        int h = 19;
        for (int j = 0 ; h < 33 && j < 14; j++, h++)
        {
          currentTime[j] = cclkOut[h];
        }

        return currentTime;
        //Serial.println("currentTie =");
        //delay(10);
        //Serial.println(currentTime);
        //delay(100);
      }
    }
  }

  //Serial.println("ERR in gsmRTC");
  //delay(10);
  loop();
}


void timerComparision(int startTime, int stopTime, int realTime, int solenoid1, int solenoid2, int solenoid3)
{
  //Serial.println("in timerComparision function");
  //delay(10);

  if (startTime != 0 && stopTime != 0)
  {

    if (realTime >= startTime && realTime <= stopTime)
    {
      //Serial.println("timer working");
      //delay(10);
      if (pumpStatus == 0)
      {
        pumpSwitch = 1;
        pump();
      }

      if (solenoid1 == 1)
      {
        digitalWrite(solenoid1_relay, HIGH);
      }

      if (solenoid1 == 0)
      {
        digitalWrite(solenoid1_relay, LOW);
      }

      if (solenoid2 == 1)
      {
        digitalWrite(solenoid2_relay, HIGH);
      }

      if (solenoid2 == 0)
      {
        digitalWrite(solenoid2_relay, LOW);
      }

      if (solenoid3 == 1)
      {
        digitalWrite(solenoid3_relay, HIGH);
      }

      if (solenoid3 == 0)
      {
        digitalWrite(solenoid3_relay, LOW);
      }

    }

    else
    {
      //Serial.println("timer stoped");

      //delay(10);
      if (pumpStatus == 1)
      {
        pumpSwitch = 0;
        pump();
      }

    }
  }
  else
  {
    //Serial.println("error: realTime = 0");
  }
}


bool deleteMessage()
{
  //Serial.println("in deleteMessage");
  //delay(10);

  for (int i = 0; i < 6; i++)
  {
    Serial1.print("AT + CMGD = 1\r\n");
    delay(500);
    String checkDelete = readSerial();
    delay(500);
    //Serial.println(checkDelete);
    //delay(50);


    if ((checkDelete.indexOf("ER")) == -1)
    {
      if ((checkDelete.indexOf("AT")) != -1)
      {
        //Serial.println("no ERR in deleteMessage");
        return true;
      }
    }

  }

  //Serial.println("error: out of deleteMessage loop");
  loop();
  //return false;
}


void smsAckSend(String acknowledgment, String phoneNoBuffer)
{
  //Serial.println("in smsAckSend");
  //delay(10);

  digitalWrite(RED_LED, HIGH);
  delay(100);
  digitalWrite(RED_LED, LOW);
  delay(100);
  digitalWrite(BLUE_LED, HIGH);
  delay(100);
  digitalWrite(BLUE_LED, LOW);
  delay(100);
  digitalWrite(GREEN_LED, HIGH);
  delay(100);
  digitalWrite(GREEN_LED, LOW);
  //delay(10);
  //Serial.println(phoneNoBuffer);
  delay(10);
  char charPhoneNo[16];
  //int j = phoneNoBuffer.indexOf("+");
  int j;
  //Serial.println("index of + =");
  //delay(10);
  //Serial.println(j);
  //delay(10);
  //Serial.println("message to send =");
  //Serial.println(acknowledgment);


  /*

    for (int v = 0; v < 80; v++)
    {
    Serial.println("v =");
    delay(10);
    Serial.println(v);
    delay(10);
    Serial.println(phoneNoBuffer[v]);
    delay(10);
    if (phoneNoBuffer[v] == '+')
    {
      Serial.println("Index of + =");
      delay(50);
      Serial.println(v);
      delay(50);
      if ((v > 34 && v < 56) || v == 0)
      {
        j = v;
      }
    }
    }

  */

  for (int i = 0; i < 14 && j < 100; i++, j++)
  {
    //Serial.println("inside loop");
    //delay(100);
    //Serial.println("i =");
    //delay(100);
    //Serial.println(i);
    //delay(100);
    //Serial.println("j =");
    //delay(100);
    //Serial.println(j);
    //delay(100);

    charPhoneNo[i] = phoneNoBuffer[j];
    //Serial.println(phoneNo[i]);
    //Serial.println(phoneNoBuffer[j]);
  }
  //delay(100);
  //Serial.println(phoneNo);

  String strPhoneNo;
  strPhoneNo = String(charPhoneNo);
  delay(100);
  //Serial.println("phoneNoBuffer =");
  //delay(100);
  //Serial.println(phoneNoBuffer);
  //delay(100);
  //Serial.println("string phone no =");
  //delay(100);
  //Serial.println(strPhoneNo);
  //delay(100);
  //Serial.println("message to send =");
  //delay(100);
  //Serial.println(acknowledgment);
  //delay(100);

  //Serial.println("sending SMS");
  //delay(10);

  Serial1.print("AT+CMGS=\"");
  delay(1000);
  Serial1.print("+918349504911");
  delay(1000);
  Serial1.println("\"\r");
  delay(1000);
  Serial1.println(acknowledgment);
  delay(100);
  Serial1.println((char)26);
  delay(100);
  //Serial.println(readSerial());
  //delay(10);
}


void pump()
{
  //Serial.println("in pump function");
  //delay(10);

  if (pumpSwitch == 1 && pumpStatus == 0 && waterAvail == 1)
  {
    digitalWrite(BLUE_LED, HIGH);

    //Serial.println("pump ON");
    //delay(10);

    digitalWrite(pumpon_relay, HIGH);
    delay(2500);
    digitalWrite(pumpon_relay, LOW);
    pumpStatus = 1;
    String smsPumpMessage = "Pump ON";
    smsAckSend(smsPumpMessage, buffer);
    delay(10);
    digitalWrite(BLUE_LED, LOW);
  }

  if (pumpSwitch == 0 && pumpStatus == 1)
  {
    digitalWrite(RED_LED, HIGH);

    //Serial.println("pump OFF");
    //delay(10);

    digitalWrite(pumpoff_relay, HIGH);
    delay(7000);
    digitalWrite(pumpoff_relay, LOW);
    pumpStatus = 0;
    String smsPumpMessage = "Pump OFF";
    smsAckSend(smsPumpMessage, buffer);
    delay(10);
    digitalWrite(RED_LED, LOW);
  }
}


char * sensorRead(String deviceID)
{
  //Serial.println("in sensorRead function");
  //delay(2000);

  static char loraByte[3];
  unsigned int i, N, count;
  char ch;
  i = 0; N = 3;
  count = 8000;

  Serial.print(deviceID);

  while (i < N && count > 0)
  {
    if (Serial.available())
    {
      ch = Serial.read();
      loraByte[i] = ch;
      //Serial.println("loraByte =");
      //Serial.println(loraByte[i]);
      i++;
      count = 8000;
    }
    else {
      delay(1);
      count--;
      //Serial.println(count);
    }
  }

  if (i == 3)
  {
    //do nothing
  }
  else
  {
    loraByte[0] = 'e';
    loraByte[1] = 'e';
    loraByte[2] = 'e';

    //Serial.println("lora error sensor value");
    //delay(10);
    //Serial.print( MyByte[i]);
    //Serial.println("MyByte =");
    //Serial.println(MyByte);
    //delay(10);
    //Serial.println("eee,eee,eee,eee,eee");
    //delay(10);
  }

  return loraByte;
}

// call this function after mdcMoistSensor/mdcBackMoistSensor have been called
void sdWrite(char deviceID)
{
  Serial.println("in sdWrite function");
  delay(10);
  char charpumpStatus = pumpStatus + '0';
  char charsolenoid1 = solenoid1 + '0';
  char charsolenoid2 = solenoid2 + '0';
  char charsolenoid3 = solenoid3 + '0';
  char charmodeIndex = modeIndex + '0';
  
  int realHours = RTChours();

  int realMinutes = RTCminutes();

  //Serial.println("realHours =");
  //Serial.println(realHours);
  //Serial.println("realinutes");
  //Serial.println(realMinutes);
  Wire.beginTransmission(8);
  Wire.write(charRTChours[0]);
  Wire.write(charRTChours[1]);
  Wire.write(charRTCminutes[0]);
  Wire.write(charRTCminutes[1]);
  Wire.write(charMDCmoistValue[0]);
  Wire.write(charMDCmoistValue[1]);
  Wire.write(charMDCmoistValue[2]);
  Wire.write(charMDCbackMoistValue[0]);
  Wire.write(charMDCbackMoistValue[1]);
  Wire.write(charMDCbackMoistValue[2]);
  Wire.write(charpumpStatus);
  Wire.write(charsolenoid1);
  Wire.write(charsolenoid2);
  Wire.write(charsolenoid3);
  Wire.write(charmodeIndex);
  Wire.endTransmission();
  
  /*
  Wire.write('0');
  Wire.write('0');
  Wire.write('0');
  Wire.write('0');
  Wire.write('0');
  Wire.endTransmission();
  */
  
  Serial.print(charRTChours[0]);
  Serial.print(charRTChours[1]);
  Serial.print(charRTCminutes[0]);
  Serial.print(charRTCminutes[1]);
  Serial.print(charMDCmoistValue[0]);
  Serial.print(charMDCmoistValue[1]);
  Serial.print(charMDCmoistValue[2]);
  Serial.print(charMDCbackMoistValue[0]);
  Serial.print(charMDCbackMoistValue[1]);
  Serial.print(charMDCbackMoistValue[2]);
  
  Serial.print(charpumpStatus);
  Serial.print(charsolenoid1);
  Serial.print(charsolenoid2);
  Serial.print(charsolenoid3);
  Serial.println(charmodeIndex);
  
}


float ultrasonicSens()
{
  //Serial.println("in ultrasonic function");
  delay(10);
  int intUltraByte;
  unsigned int i, N, count;
  char ch;
  i = 0; N = 1;
  count = 8000;

  Wire.requestFrom(8, 5);

  while (i < N && count > 0)
  {
    if (Wire.available())
    {
      intUltraByte = Wire.read();
      //Serial.println("UltraByte =");
      //Serial.println(intUltraByte);
      i++;
      count = 8000;
    }
    else {
      delay(1);
      count--;
      //Serial.println(count);
    }
  }
  
  //Serial.println("i =");
  //Serial.println(i);
  if (i == 1)
  {
    //ultraSensValue = atol(charUltraByte);
    
    //Serial.println("ultraSensValue =");
    //Serial.println(intUltraByte);
    
    if (intUltraByte >= 3000) {
      waterAvail = 0;

      if (waterLowAck == 0) {
        smsAckSend("ALERT: water level low", "+918349504911");
      }

      waterLowAck++;
    }
    else {
      waterAvail = 1;
      waterLowAck = 0;
    }
  }
  else
  { /*
    charUltraByte[0] = 'e';
    charUltraByte[1] = 'e';
    charUltraByte[2] = 'e';
    charUltraByte[3] = 'e';
    charUltraByte[4] = 'e';
    */
  }

}

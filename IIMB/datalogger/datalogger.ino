/***********************************************************************
  SD card read/write
  This example shows how to read and write data to and from an SD card file
  The circuit:
  SD card attached to SPI bus as follows:
  MOSI - pin 11
  MISO - pin 12
  CLK - pin 13
  CS - pin 4
**********************************************************************/

/**********************************************************************

  1. SD.remove("file_name");  //write new data every time
  2. myFile.seek(position); //set the cursor at particular position to write the data
   [0th byte-> 1st character, 1st-byte-> 2nd character]

***********************************************************************/

#include <SD.h>
#include <SoftwareSerial.h>

// software serial #1: RX = digital pin 5, TX = digital pin 6
SoftwareSerial sensPort(5, 6);

char sensID1 = 'm';
char sensID2 = 'b';

File myFile;
String data = "write your data here";

void setup()
{
  Serial.begin(9600);  // Open serial communications and wait for port to open:
  sensPort.begin(9600);

  Serial.print("Initializing SD card...");
  //pinMode(10, OUTPUT);

  if (!SD.begin(4)) {
    Serial.println("initialization failed!");
    return;
  }
  else
  {

  }
}


/************************loop_function*************************/
void loop()
{
  checkSerial(sensID1, sensID2);
}


/*----------check device ID in serial buffer----------*/
void checkSerial(char deviceID1, char deviceID2)
{
  Serial.println("in checkSerial fucntion");
  char cmd;

  sensPort.listen();

  if (sensPort.available()) // wait for software UART
  {
    cmd = sensPort.read();

    if (cmd == deviceID2)
    {
      Serial.println("deviceID = b cmd found");
      dataFetcher(deviceID2);
    }
    
    if (cmd == deviceID1)
    {
      Serial.println("deviceID = m cmd found");
      dataFetcher(deviceID1);
    }

  }

  //dataFetcher(deviceID1);
  //dataFetcher(deviceID2);

  while (sensPort.available() == 0)
  {
    //do nothing
  }
}


void dataFetcher(char deviceID)
{
  Serial.println("In dataFetcher function");
  char charMDCdata[4];
  char charMDCbackData[4];
  int N = 3;
  int k = 0;
  int count = 8000;
  //char testCharMDCdata[9] = {'7', ':', '5', '6', ' ', 'e', 'e', 'e'};
  //char testCharMDCbackData[9] = {'9', ':', '3', '2', ' ', 'e', 'e', 'e'};

  if (deviceID == 'm') {

    while (k < N && count > 0)
    {
      if (sensPort.available())
      {
        charMDCdata[k] = sensPort.read();
        k++;
        count = 8000;
      }
      else {
        delay(1);
        count--;
      }
    }

    if (k == 3)
    {
      Serial.println("got 3 characters");
      //do nothing
    }
    else
    {
      charMDCdata[0] = 'e';
      charMDCdata[1] = 'r';
      charMDCdata[2] = 'r';
      Serial.println("no data received");
    }

    charMDCdata[3] = '\0'; // to remove the garbage value added at the end
    Serial.println("calling sdWrite function");
    sdWrite(String(deviceID), String(charMDCdata));
  }

  k = 0;
  N = 3;
  count = 8000;
  if (deviceID == 'b') {

    while (k < N && count > 0)
    {
      if (sensPort.available())
      {
        charMDCbackData[k] = sensPort.read();
        k++;
        count = 8000;
      }
      else {
        delay(1);
        count--;
      }
    }

    if (k == 3)
    {
      Serial.println("got 3 characters");
      //do nothing
    }
    else
    {
      charMDCbackData[0] = 'e';
      charMDCbackData[1] = 'r';
      charMDCbackData[2] = 'r';
      Serial.println("no data received");
    }

    charMDCbackData[3] = '\0'; // to remove the garbage value added at the end
    sdWrite(String(deviceID), String(charMDCbackData));
  }
}


/**********************Write_function***************************/
void sdWrite(String deviceID, String sensData)
{
  Serial.println("deviceID =");
  Serial.println(deviceID);
  Serial.println("sensData =");
  Serial.println(sensData);
  Serial.println("initialization done.");

  myFile = SD.open(deviceID, FILE_WRITE);

  if (myFile)
  {
    Serial.print("Writing to file...");
    myFile.println(sensData);
    myFile.close();
    Serial.println("done.");
  }
  else
  {
    Serial.println("error opening");
  }
}




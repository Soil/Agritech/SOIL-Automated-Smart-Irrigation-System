#include <SD.h>

#include <Wire.h>
#include <LiquidCrystal.h>

const int rs = 7, en = 6, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

#define trigger 8
#define echo 9
float depth;
float time = 0, distance = 0;
char charDepth[5];
char receiveByte[15] = {'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i'};
char charRTCHours[2];
char charRTCMinutes[2];
char charMDCmoist[3];
char charMDCbackMoist[3];
char charPumpStatus[1];
char charAmphiStatus[1];
char charMDCstatus[1];
char charMDCbackStatus[1];
char modeIndex[1];

File myFile;

void setup() {
  Serial.begin(9600);        // start serial for output

  pinMode(trigger, OUTPUT);
  pinMode(echo, INPUT);

  Serial.println("Initializing SD card...");
  if (!SD.begin(10))
  {
    Serial.println("SD initialization failed!");
  }
  else
  {
    Serial.println("SD initialization is done");
  }

  lcd.begin(16, 2);

  Wire.begin(8);                // join i2c bus with address #8
  Wire.onReceive(receiveEvent);
  Wire.onRequest(requestEvent);
}


void loop() {
  //Serial.println("in loop function");
  /*
  depth = measureDepth();
  dtostrf(depth, 3, 1, charDepth); //converts the float or integer to a string. (floatVar, minStringWidthIncDecimalPoint, numVarsAfterDecimal, empty array)
  Serial.println("charDepth =");
  Serial.print(charDepth[0]);
  Serial.print(charDepth[1]);
  Serial.print(charDepth[2]);
  Serial.print(charDepth[3]);
  Serial.print(charDepth[4]);
  */
  
  
}


// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany) {

  int i = 0; //counter for each bite as it arrives
  Serial.println("received bytes =");
  while (Wire.available()) {
    receiveByte[i] = Wire.read(); // every character that arrives it put in order in the empty array "receiveByte"
    Serial.println(receiveByte[i]);
    i++;
  }
  dataPacket();
  LCD();
  sdWrite();
}


void requestEvent()
{ 

  
  //char intCharDepth;
  Serial.println("request for depth");
  
  measureDepth();

  Serial.println("intDistance =");
  Serial.println(int(distance));
    
  Wire.write(int(distance));
}


float measureDepth()
{
  Serial.println("in measureDepth function");
  digitalWrite(trigger, LOW);
  delayMicroseconds(2);
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);
  delayMicroseconds(2);

  time = pulseIn(echo, HIGH);
  distance = time * 340 / 20000;
  delay(100);

  Serial.println("Depth =");
  Serial.println(distance);
  delay(300);
}


void dataPacket()
{
  Serial.println("in dataPacket func");
  charRTCHours[0] = receiveByte[0];
  charRTCHours[1] = receiveByte[1];

  charRTCMinutes[0] = receiveByte[2];
  charRTCMinutes[1] = receiveByte[3];

  charMDCmoist[0] = receiveByte[4];
  charMDCmoist[1] = receiveByte[5];
  charMDCmoist[2] = receiveByte[6];

  charMDCbackMoist[0] = receiveByte[7];
  charMDCbackMoist[1] = receiveByte[8];
  charMDCbackMoist[2] = receiveByte[9];

  charPumpStatus[0] = receiveByte[10];

  charAmphiStatus[0] = receiveByte[11];

  charMDCstatus[0] = receiveByte[12];

  charMDCbackStatus[0] = receiveByte[13];

  modeIndex[0] = receiveByte[14];
}


void LCD()
{
  Serial.println("in LCD func");

  lcd.setCursor(0, 0);

  if (charAmphiStatus[0] == '1') {
    lcd.print("Amphi ON Mode=");
    lcd.print(modeIndex[0]);
    lcd.print(" ");
  }

  if (charMDCstatus[0] == '1') {
    lcd.print("MDC ON Mode=");
    lcd.print(modeIndex[0]);
    lcd.print("   ");
  }

  if (charMDCbackStatus[0] == '1') {
    lcd.print("Mback ON Mode=");
    lcd.print(modeIndex[0]);
    lcd.print(" ");
  }

  else {
    lcd.print("Pump off Mode=");
    lcd.print(modeIndex[0]);
    lcd.print(" ");
  }



  lcd.setCursor(0, 1);

  lcd.print("M");
  lcd.print(charMDCmoist[0]);
  lcd.print(charMDCmoist[1]);
  lcd.print(charMDCmoist[2]);
  lcd.print("%");

  lcd.print(" ");

  lcd.print("B");
  lcd.print(charMDCbackMoist[0]);
  lcd.print(charMDCbackMoist[1]);
  lcd.print(charMDCbackMoist[2]);
  lcd.print("%");

  lcd.print(" ");
  lcd.print("W");
  lcd.print(int(distance));
  lcd.print("   ");
  
}


void sdWrite()
{
  Serial.println("in sdWrite function");

  myFile = SD.open("data.txt", FILE_WRITE);

  if (myFile)
  {
    Serial.print("Writing to file...");
    myFile.print(charRTCHours[0]);
    myFile.print(charRTCHours[1]);
    myFile.print(":");
    myFile.print(charRTCMinutes[0]);
    myFile.print(charRTCMinutes[1]);
    myFile.print(" ");
    myFile.print(charMDCmoist[0]);
    myFile.print(charMDCmoist[1]);
    myFile.print(charMDCmoist[2]);
    myFile.print("%");
    myFile.print(" ");
    myFile.print(charMDCbackMoist[0]);
    myFile.print(charMDCbackMoist[1]);
    myFile.print(charMDCbackMoist[2]);
    myFile.print("%");
    myFile.print(" ");

    if (charPumpStatus[0] == '1') {
      myFile.print("ON");
    }
    else {
      myFile.print("OFF");
    }

    myFile.print(" ");

    if (charAmphiStatus[0] == '1') {
      myFile.print("ON");
    }
    else {
      myFile.print("OFF");
    }

    myFile.print(" ");

    if (charMDCstatus[0] == '1') {
      myFile.print("ON");
    }
    else {
      myFile.print("OFF");
    }

    myFile.print(" ");

    if (charMDCbackStatus[0] == '1') {
      myFile.print("ON");
    }
    else {
      myFile.print("OFF");
    }

    myFile.print(" ");
    myFile.print(modeIndex[0]);

    myFile.print(" ");
    myFile.println(distance);
    
    myFile.close();
    
    Serial.println("done.");
  }
  else
  {
    Serial.println("error opening");
  }
}

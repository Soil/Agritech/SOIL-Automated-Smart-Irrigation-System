#include <SoftwareSerial.h>
#define loraLEDpin A1

/********LORA(TX,RX)********/
SoftwareSerial LORA(6,7);

void setup()
{
  Serial.begin(9600); // Back-channel UART port init for debugging
  LORA.begin(9600); // LoRa serial port init

  pinMode(loraLEDpin,OUTPUT);

  digitalWrite(loraLEDpin,HIGH);

  Serial.println("LoRa Started");
  delay(500);

  digitalWrite(loraLEDpin,LOW);
}

void loop()
{
  loraRead();
}

void loraRead()
{
  char ch; // stores the character received on serial port of LoRa

  if(LORA.available())
  {
    digitalWrite(loraLEDpin,HIGH);

    ch=LORA.read();

    // it checks if LoRa is receiving 'R' character from the Hub module, if received, then send 'S' character as an acknowledgment
    if(ch=='R')
    {
      Serial.println(ch);
      delay(500);
      LORA.write('S');
    }

    digitalWrite(loraLEDpin,LOW);
  }
}


#include <SoftwareSerial.h>
#include <LiquidCrystal.h>

#define gsmLEDpin A0  //Led indication of GSM
#define loraLEDpin A1 //Led indication of LoRa
#define switchPin 8    //Switch for Change Mode

const int rs = 12, en = 13, d4 = 5, d5 = 4, d6 = 3, d7 = 2; // pins for LCD
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

String s, d = "SOIL AGRITECH", a = "System Ready"; // init strings to be printed on LCD

// flags to check that "IN GSM MODE" and "IN LoRa MODE" messages are only printed when there is a change of mode
int gsmModeLCDflag, loraModeLCDflag;

bool sw; // variable to store status of switchPin

/********GSM(TX,RX)********/
SoftwareSerial GSM(9, 10);

/********LORA(TX,RX)********/
SoftwareSerial LORA(11, 6);

void setup()
{
  Serial.begin(9600); // Back-channel UART port init for debugging
  LORA.begin(9600); // LoRa serial port init
  GSM.begin(9600); // GSM serial port init

  pinMode(gsmLEDpin, OUTPUT);
  pinMode(loraLEDpin, OUTPUT);
  pinMode(switchPin, INPUT_PULLUP);  // Detecting active-LOW signal to change Mode

  digitalWrite(gsmLEDpin, HIGH);
  digitalWrite(loraLEDpin, HIGH);

  lcd.begin(16, 2);
  lcd.setCursor(1, 0);

  // loop to print "SOIL AGRITECH" message on LCD
  for (int i = 0; i < 13; i++)
  {
    lcd.print(d[i]);
    delay(110);
  }
  delay(300);
  lcd.setCursor(1, 1);

  // loop to print "System Ready" message on LCD
  for (int i = 0; i < 12; i++)
  {
    lcd.print(a[i]);
    delay(110);
  }

  delay(2000);
  digitalWrite(gsmLEDpin, LOW);
  digitalWrite(loraLEDpin, LOW);
  lcd.clear();
}

void loop()
{
  Serial.println("test serial");
  sw = digitalRead(switchPin); // Stores the status of switch

  if (sw == 0)
  {
    loraRead();
  }
  else
  {
    gsmRead();
  }
}

/********GSM Read Function*********/
void gsmRead()
{
  /*
  RSSI is Received Signal Strength Indication.
   
   Value 	RSSI dBm 	Condition
   2 	         -109 	        Marginal
   3 	         -107 	        Marginal
   4 	         -105 	        Marginal
   5 	         -103           Marginal
   6 	         -101           Marginal
   7 	         -99 	        Marginal
   8 	         -97 	        Marginal
   9 	         -95 	        Marginal
   10 	         -93 	          OK
   11 	         -91 	          OK
   12 	         -89 	          OK
   13 	         -87 	          OK
   14 	         -85 	          OK
   15 	         -83 	         Good
   16 	         -81 	         Good
   17 	         -79 	         Good
   18 	         -77 	         Good
   19 	         -75 	         Good
   20 	         -73 	        Excellent
   21 	         -71 	        Excellent
   22 	         -69 	        Excellent
   23 	         -67 	        Excellent
   24 	         -65 	        Excellent
   25 	         -63 	        Excellent
   26 	         -61 	        Excellent
   27 	         -59 	        Excellent
   28 	         -57 	        Excellent
   29 	         -55 	        Excellent
   30 	         -53 	        Excellent
   */

  // BER is the Bit Error Rate. (0-7 scale)

  Serial.println("IN GSM mode");
  
  String gsmBuffer;
  int f,c; // stores the index of RSSI(Received Signal Strength Indication) values in gsmBuffer string
  int ber; // stores the index of BER(Bit Error Rate) value in gsmBuffer string
  int x;
  char charRSSI[3];
  int intRSSI;

  loraModeLCDflag = 0;

  if (gsmModeLCDflag == 0)
  {
    lcd.clear();
    lcd.setCursor(2, 0);
    lcd.print("IN GSM MODE");
    delay(2000);
    lcd.clear();
    gsmModeLCDflag++;
  }
  
  Serial.println("1");
  
  lcd.setCursor(0, 0);
  lcd.print("RSSI =");

  Serial.println("test serial");
  
  GSM.listen();
  GSM.write("AT+CSQ\r"); // It checks the signal strength of the network
  delay(200);

  Serial.println("2");
  
  if (GSM.available())
  {
    Serial.println("3");
    
    gsm_led();

    gsmBuffer = GSM.readString();
    Serial.print("gsmBuffer = ");
    delay(10);
    Serial.println(gsmBuffer);

    f = gsmBuffer.indexOf(':')+2;
    c = gsmBuffer.indexOf(':')+3;
    ber = gsmBuffer.indexOf(':')+5;

    charRSSI[0] = gsmBuffer[f];
    charRSSI[1] = gsmBuffer[c];
    charRSSI[2] = '\0';

    // Converts RSSI value(characters) into integer
    intRSSI = atol(charRSSI);
    Serial.println("intRSSI = ");
    Serial.println(intRSSI);

    // if the RSSI value is single digit then shift index for BER by one position
    if (intRSSI < 10)
    {
      ber = gsmBuffer.indexOf(':')+4;
    }
    else
    {
      ber = gsmBuffer.indexOf(':')+5;
    }

    lcd.setCursor(0, 0);
    lcd.print("RSSI =");
    lcd.setCursor(7, 0);
    lcd.print(gsmBuffer[f]);
    lcd.print(gsmBuffer[c]);

    lcd.setCursor(10, 0);
    lcd.print("BER(");
    lcd.setCursor(14,0);
    lcd.print(gsmBuffer[ber]);
    lcd.print(')');

    lcd.setCursor(0, 1);
    lcd.print("Strength");
    lcd.setCursor(9,1);

    if (intRSSI >= 2 && intRSSI <= 9)
    {
      lcd.print("-> BAD ");
    }

    if (intRSSI >= 10 && intRSSI <= 14)
    {
      lcd.print("-> OK  ");
    }

    if (intRSSI >= 15 && intRSSI <= 19)
    {
      lcd.print("-> GOOD");
    }

    if (intRSSI >= 20 && intRSSI <= 30)
    {
      lcd.print("-> BEST");
    }


    gsm_led();
  }
}


/********LoRa Read Function*********/
void loraRead()
{
  Serial.println("in loRa mode");
  
  unsigned int count = 8000;
  char ch; // stores the character received on serial port of LoRa

  gsmModeLCDflag = 0;

  if (loraModeLCDflag == 0)
  {
    lcd.clear();
    lcd.setCursor(2, 0);
    lcd.print("IN LoRa MODE");
    delay(2000);
    lcd.clear();
    loraModeLCDflag++;
  }

  LORA.listen();
  LORA.write('R'); // Sending 'R' character to the node module

    // This loop waits for 8000 milli seconds to check whether it is getting acknowledgment from the node module
  while (count >= 0)
  {
    if (LORA.available() > 0)
    {
      ch = LORA.read();

      if (ch == 'S') // it checks if LoRa is receiving 'S' character from the node module
      {
        lora_led();
        lcd.clear();
        lcd.setCursor(6, 0);
        lcd.print("LoRa");
        lcd.setCursor(4, 1);
        lcd.print("Available");
        delay(80);
        lora_led();
        break;
      }
    }

    else if (count == 0)
    {
      digitalWrite(loraLEDpin, LOW);
      lcd.clear();
      lcd.setCursor(6, 0);
      lcd.print("LoRa");
      lcd.setCursor(2, 1);
      lcd.print("OUT OF RANGE");
      break;
    }

    else
    {
      delay(1);
      count--;
    }
  }
}


/********GSM LED Function*********/
void gsm_led()
{
  digitalWrite(gsmLEDpin, HIGH);
  delay(50);
  digitalWrite(gsmLEDpin, LOW);
}


/********LoRa LED Function*********/
void lora_led()
{
  digitalWrite(loraLEDpin, HIGH);
  delay(50);
  digitalWrite(loraLEDpin, LOW);
}


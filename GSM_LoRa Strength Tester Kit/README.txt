GSM & LoRa Strength Tester Kit (14/03/2018)
-------------------------------------------------------------------------------------------------------------------
# IMPORTANT:
(Vcc & Gnd is taken directly from Arduino Uno USB)

-------------------------------------------------------------------------------------------------------------------
/*.....HUB CONNECTIONS.....*/

# LCD connections:
    rs --> 12
    en --> 11
    D4 --> 5
    D5 --> 4
    D6 --> 3
    D7 --> 2

# GSM connections:
    Tx --> 9
    Rx --> 10
    GSM Led --> A0

# LoRa connections:
    Tx --> 11
    Rx --> 6
    GSM Led --> A1

# Switch connection:(pull up resistor)
    Gnd <-- switch --> pin 8

--------------------------------------------------------------------------------------------------------------------
/*.....NODE CONNECTIONS.....*/

# LoRa connections:
    Tx --> 6
    Rx --> 7
    GSM Led --> A1

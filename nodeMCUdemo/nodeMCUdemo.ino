#include <ESP8266WiFi.h>

//////////////////////
// WiFi Definitions //
//////////////////////
const char WiFiAPPSK[] = "pass1234";

const int pumpoff_relay = D2;
const int pumpon_relay = D0
                         ;

const int RED_LED = D9;
const int GREEN_LED = D7;

/////////////////////
// Pin Definitions //
/////////////////////
const int LED_PIN = 5; // Thing's onboard, green LED
const int ANALOG_PIN = A0; // The only analog pin on the Thing
const int DIGITAL_PIN = 12; // Digital pin to be read

int moistValue; // Value of the moisture sensor

WiFiServer server(80);

void setup()
{
  initHardware();
  setupWiFi();
  server.begin();
}

void loop()
{

  
  
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }

  // Read the first line of the request
  String req = client.readStringUntil('\r');
  Serial.println(req);
  client.flush();

  // Match the request
  int val = -1; // We'll use 'val' to keep track of both the
  // request type (read/set) and value if set.
  if (req.indexOf("/relay/0") != -1)
  {
    digitalWrite(RED_LED, HIGH);
    digitalWrite(GREEN_LED, LOW);
    digitalWrite(pumpoff_relay, HIGH);
    delay(7000);
    digitalWrite(pumpoff_relay, LOW);
    digitalWrite(RED_LED, LOW);
    val = 0; // Will write LED low
    Serial.println("relay OFF");
  }

  else if (req.indexOf("/relay/1") != -1)
  {
    digitalWrite(GREEN_LED, HIGH);
    digitalWrite(RED_LED, LOW);
    digitalWrite(pumpon_relay, HIGH);
    delay(3000);
    digitalWrite(pumpon_relay, LOW);
    digitalWrite(GREEN_LED, LOW);
    val = 1; // Will write LED high
    Serial.println("relay ON");
  }

  else if (req.indexOf("/read") != -1)
  {
    val = -2; // Will print pin reads
    moistValue = moistRead();
  }
  // Otherwise request will be invalid. We'll say as much in HTML

  // Set GPIO5 according to the request
  //if (val >= 0)
  //  digitalWrite(LED_PIN, val);

  client.flush();

  // Prepare the response. Start with the common header:
  String s = "HTTP/1.1 200 OK\r\n";
  s += "Content-Type: text/html\r\n\r\n";
  s += "<!DOCTYPE HTML>\r\n<html>\r\n";
  // If we're setting the LED, print out a message saying we did
  if (val >= 0)
  {
    s += "Relay is now ";
    s += (val) ? "on" : "off";
  }

  else if (val == -2)
  { // If we're reading pins, print out those values:
    //s += "Analog Pin = ";
    s += String(moistValue);
    s += "#";
    //s += "Digital Pin 12 = ";
    //s += String(digitalRead(DIGITAL_PIN));
  }


  else
  {
    s += "Invalid Request.<br> Try /relay/0 or /relay/1";
  }
  s += "</html>\n";

  // Send the response to the client
  client.print(s);
  delay(1);
  Serial.println("Client disonnected");

  // The client will actually be disconnected
  // when the function returns and 'client' object is detroyed
}

void setupWiFi()
{
  WiFi.mode(WIFI_AP);

  // Do a little work to get a unique-ish name. Append the
  // last two bytes of the MAC (HEX'd) to "Thing-":
  uint8_t mac[WL_MAC_ADDR_LENGTH];
  WiFi.softAPmacAddress(mac);
  String macID = String(mac[WL_MAC_ADDR_LENGTH - 2], HEX) +
                 String(mac[WL_MAC_ADDR_LENGTH - 1], HEX);
  macID.toUpperCase();
  String AP_NameString = "ESP8266 Thing " + macID;

  char AP_NameChar[AP_NameString.length() + 1];
  memset(AP_NameChar, 0, AP_NameString.length() + 1);

  for (int i = 0; i < AP_NameString.length(); i++)
    AP_NameChar[i] = AP_NameString.charAt(i);

  WiFi.softAP(AP_NameChar, WiFiAPPSK);
}

void initHardware()
{
  Serial.begin(115200);
  pinMode(DIGITAL_PIN, INPUT_PULLUP);
  pinMode(LED_PIN, OUTPUT);
  pinMode(pumpon_relay, OUTPUT);
  pinMode(pumpoff_relay, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);
  pinMode(RED_LED, OUTPUT);
  digitalWrite(pumpoff_relay, LOW);
  digitalWrite(pumpon_relay, LOW);
  digitalWrite(LED_PIN, LOW);
  digitalWrite(GREEN_LED, LOW);
  digitalWrite(RED_LED, LOW);

  // Don't need to set ANALOG_PIN as input,
  // that's all it can be.
}

int moistRead()
{
  int mositure1value;
  int MS1;
  
  mositure1value = analogRead(ANALOG_PIN);
  
  MS1 = mositure1value;
  
  if (MS1 < 340) {
    MS1 = 340;
  }
  if (MS1 > 680) {
    MS1 = 680;
  }

  MS1 = (MS1 - 340) * 10;
  MS1 = 100 - (MS1 / 34);

  return MS1;
}


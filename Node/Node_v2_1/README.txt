Node_v2_1 (Date: 05/02/2018)
//---------------------------------------------------------//
$ Added different fuctions(different characters sent to node) to send different sensor values separately
  to overcome the data re-shuffling

$ Compatible with Hub_v2_1

$ Sensor data jumbling bug at Lora modem fixed.(Packet Identifier '#' added when sending the sensor data)

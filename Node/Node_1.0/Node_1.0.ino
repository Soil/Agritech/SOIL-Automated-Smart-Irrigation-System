/*--------------------------Program for Node --------------------------------------------------*/
#include "DHT.h"

#define DHTPIN 31
#define DHTTYPE DHT11

const int mosituresensor1   = A0;
const int mosituresensor2   = A1;
const int temperaturesensor = A6;

int mositure1value   = 0;
int mositure2value   = 0;
int temperaturevalue = 0;

unsigned int MS1 = 0;
unsigned int MS2 = 0;
float temp = 0;
unsigned int TS1 = 0;
char cmd;

int mydata[30];

float h, f, t, hif, hic;
unsigned int tp, hy;

DHT dht(DHTPIN, DHTTYPE);
/*------------------------setup function ----------------------------------------------------------*/
void setup()
{
      Serial1.begin(9600);
      Serial.begin(9600);
      dht.begin();

      Serial.println("test serial");
      delay(10);

      analogReadResolution(14);

      /*----------------mositure sensor 1-------------------------------*/
      mositure1value = analogRead(mosituresensor1);
      MS1 = mositure1value;
      if (MS1 < 380) {
            MS1 = 380;
      }
      if (MS1 > 800) {
            MS1 = 800;
      }

      MS1 = (MS1 - 380) * 10;
      MS1 = 100 - (MS1 / 42);

      /*----------------mositure sensor 2-------------------------------*/
      mositure2value = analogRead(mosituresensor2);
      MS1 = mositure2value;
      if (MS1 < 6360) {
            MS1 = 6360;
      }
      if (MS1 > 13000) {
            MS1 = 13000;
      }

      MS1 = (MS1 - 6360) * 10;
      MS1 = 100 - (MS1 / 665);

      /*----------------Temperature sensor 1-------------------------------*/
      temperaturevalue = analogRead(temperaturesensor);
      temp = temperaturevalue;
      temp = (5.0 * temp * 100.0) / 1024;
      TS1 = (temp - 15) * 10;

}

/*------------------------Loop function-------------------------------------------------------------*/
void loop()
{

      if (Serial1.available())              // wait for hardware UART
      {
            cmd = Serial1.read();
            if (cmd == 'D') {
                  sensorread();
            }
      }


      //sensorread();



      while (Serial1.available() == 0)
      {
            //Serial1.print("hgjhghjghjghjgjhgjhg");
            //delay(2000);
            //Serial.println("ghgjhghjghjghjghghj");
            //delay(2000);
            //digitalWrite(RED_LED, HIGH);
            //delay(1000);
            //digitalWrite(RED_LED, LOW);
            //delay(1000);
      }



}

/*-------------------------sensorread function---------------------------------------------------------*/

void sensorread()
{
      int i;
      digitalWrite(RED_LED, HIGH);
      //Serial.println("in serialRead");
      //delay(10);

      for (i = 0; i < 15; i++)
      {
            mydata[i] = 's';
            //Serial.println("in first for loop");
            //delay(10);
      }

      //Serial.println("moisture 1 reading");
      //delay(10);
      // mositure sensor 1

      mositure1value = analogRead(mosituresensor1);
      Serial.print("analog moist1 =");
      delay(500);
      Serial.println(mositure1value);
      delay(500);

      MS1 = mositure1value;
      if (MS1 < 380) {
            MS1 = 380;
      }
      if (MS1 > 800) {
            MS1 = 800;
      }

      MS1 = (MS1 - 380) * 10;
      MS1 = 100 - (MS1 / 42);



      //if (MS1 > 999) {
      //MS1 = 0;
      //}

      //mydata[0] = MS1 / 100;
      //mydata[1] = (MS1 - (mydata[0] * 100)) / 10;
      //mydata[2] = MS1 - ((mydata[0] * 100) + mydata[1] * 10 );
      mydata[0] = 0;
      mydata[1] = 0;
      mydata[2] = 0;

      //Serial.print("moisture 1 =");
      //delay(500);
      //Serial.print(mydata[0]);
      //delay(500);
      //Serial.print(mydata[1]);
      //delay(500);
      //Serial.println(mydata[2]);

      //delay(100);
      //Serial.println("moisture 2 reading");
      //delay(10);
      // mositure sensor 2

      mositure2value = analogRead(mosituresensor2);



      Serial.print("analog moist2 =");
      delay(500);
      Serial.println(mositure2value);
      delay(500);

      MS1 = mositure2value;
      if (MS1 < 6360) {
            MS1 = 6360;
      }
      if (MS1 > 13000) {
            MS1 = 13000;
      }

      MS1 = (MS1 - 6360) * 10;
      MS1 = 100 - (MS1 / 665);

      //if (MS1 > 999) {
      //  MS1 = 0;
      //}

      mydata[3] = MS1 / 100;
      mydata[4] = (MS1 - (mydata[3] * 100)) / 10;
      mydata[5] = MS1 - ((mydata[3] * 100) + mydata[4] * 10 );
      Serial.print("moisture 2 =");
      delay(500);
      Serial.print(mydata[3]);
      delay(500);
      Serial.print(mydata[4]);
      delay(500);
      Serial.println(mydata[5]);


      delay(100);


      MS2 = mositure2value;

      mydata[6] = MS2 / 10000;
      mydata[7] = (MS2 - (mydata[6] * 10000)) / 1000;
      mydata[8] = (MS2 - (mydata[6] * 10000 + mydata[7] * 1000)) / 100;
      mydata[9] = (MS2 - (mydata[6] * 10000 + mydata[7] * 1000 + mydata[8] * 100)) / 10;
      mydata[10] = MS2 - (mydata[6] * 10000 + mydata[7] * 1000 + mydata[8] * 100 + mydata[9] * 10);

      /*
      mydata[6] = 0;
      mydata[7] = 0;
      mydata[8] = 0;
      mydata[9] = 0;
      mydata[10] = 0;
      */
      //Serial.println("temperature reading");
      //delay(10);
      // Temperature sensor

      temperaturevalue = analogRead(temperaturesensor);



      //Serial.println()

      temp = temperaturevalue;
      temp = (15.0 * temp * 100.0) / 1024;
      TS1 = 1351 - temp;

      if (TS1 > 999) {
            TS1 = 0;
      }

      //mydata[6] = TS1 / 100;
      //mydata[7] = (TS1 - (mydata[6] * 100)) / 10;
      //mydata[8] = TS1 - ((mydata[6] * 100) + mydata[7] * 10 );




      /*

      Serial.println("mydata analog =");
      Serial.println(mydata[6]);
      Serial.println(mydata[7]);
      Serial.println(mydata[8]);
      Serial.println(mydata[9]);
      Serial.println(mydata[10]);
      */


      delay(100);

      //Serial.println("humidity reading");
      //delay(10);
      //humidity sensor
      /*
      h = dht.readHumidity();
      // Read temperature as Celsius (the default)
      t = dht.readTemperature();
      // Read temperature as Fahrenheit (isFahrenheit = true)
      f = dht.readTemperature(true);
      // Check if any reads failed and exit early (to try again).
      if (isnan(h) || isnan(t) || isnan(f)) {
      Serial.println("Failed to read from DHT sensor!");
      return;
}

// Compute heat index in Fahrenheit (the default)
hif = dht.computeHeatIndex(f, h);
// Compute heat index in Celsius (isFahreheit = false)
hic = dht.computeHeatIndex(t, h, false);

hy = h;
tp = t;

if (hy > 999) {
hy = 0;
}

mydata[9] = hy / 100;
mydata[10] = (hy - (mydata[9] * 100)) / 10;
mydata[11] = hy - ((mydata[9] * 100) + mydata[10] * 10 );

if (tp > 999) {
tp = 0;
}

mydata[12] = tp / 100;
mydata[13] = (tp - (mydata[12] * 100)) / 10;
mydata[14] = tp - ((mydata[12] * 100) + mydata[13] * 10 );


*/

//mydata[9] = 0;
//mydata[10] = 0;
mydata[11] = 0;
mydata[12] = 0;
mydata[13] = 0;
mydata[14] = 0;
digitalWrite(RED_LED, LOW);
//Serial.println("before forming data");
//Serial.print(mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]);

for (i = 0; i < 15; i++)
{
      //Serial.println("forming data packet");
      //delay(500);
      //Serial.println("i =");
      //Serial.println(i);
      //delay(500);
      Serial1.print(mydata[i]);
      //delay(500);
      //Serial.println("data =");
      //Serial.println(mydata[i]);
      //delay(500);
      if (i == 2 || i == 5 || i == 8 || i == 11)
      {
            Serial1.print(",");
      }

      //Serial1.print(mydata[i]);
}


}

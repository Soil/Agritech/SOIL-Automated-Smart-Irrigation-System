#include <Wire.h>
#include <LiquidCrystal_I2C.h>

 


LiquidCrystal_I2C lcd(0x3F,20,4);  // set the LCD address to 0x20 for a 20 chars and 4 line display

void setup()
{
  lcd.init();                      // initialize the lcd 
 
  // Print a message to the LCD.
  lcd.backlight();
  lcd.print("43oh.com rocks!     ");
  lcd.print("Now for some RFID...");
}

void loop()
{
}

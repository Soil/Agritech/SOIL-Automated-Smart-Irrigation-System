#include<SoftwareSerial.h>
SoftwareSerial Serial1(9, 10);
int flushtime = 5000;
int timeout;

void setup()
{
  Serial.begin(9600);
  Serial1.begin(9600);

  Serial.println("test serial");
}

void loop()
{
  Serial.println("in loop");
  delay(10);
  GPRSserverUpdate();
}



void GPRSserverUpdate()
{
  ATinitiation();
  CIPSHUT();
  CIPMUX();
  CGATT();
  CSTT();
  CIICR();
  CIFSR();
  CIPSTART();
  CIPSEND();
  return;
}


String readSerial()
{
  timeout = 0;
  while  (!Serial1.available() && timeout < 12000)
  {
    delay(13);
    timeout++;
  }

  if (Serial1.available())
  {
    return Serial1.readString();
  }
}


void flushReceive(long time)
{
  int time_start = millis();
  while (Serial1.available() > 0 || (millis() - time_start < time))
  {
    delay(10);
    if (Serial1.available() > 0)
    {
      char c = Serial1.read();
      if (c == '\r' || c == '\n')
      {
        continue;
      }
    }
  }
}

bool ATinitiation()
{
  Serial.println("inside ATinitiation");
  delay(10);
  String ATcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    Serial1.print("AT\r");
    delay(500);
    ATcheck = readSerial();
    Serial.println(ATcheck);
    delay(10);

    if ((ATcheck.indexOf("ER")) == -1)
    {
      if ((ATcheck.indexOf("AT")) != -1)
      {
        Serial.println("no ERR in ATinitiation");
        delay(10);

        return true;
      }
    }

  }
  Serial.println("out of AT");
  delay(10);
  loop();
}

bool ATinitiation()
{
  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    Serial1.print("AT\r");
    delay(500);

    if ((readSerial().indexOf("ER")) == -1)
    {
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }

  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}

bool CIPSHUT()
{
  Serial.println("inside CIPSHUT");
  delay(10);
  String CIPSHUTcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    Serial1.print("AT+CIPSHUT\r\n");
    delay(500);
    CIPSHUTcheck = readSerial();
		Serial.println(CIPSHUTcheck);
		delay(10);

    if ((readSerial().indexOf("ER")) == -1)
    {
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }

  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}

bool CIPMUX()
{
  Serial.println("inside CIPMUX");
  delay(10);
  String CIPMUXcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    Serial1.print("AT+CIPMUX=0\r\n");
    delay(500);
    CIPMUXcheck = readSerial();
		Serial.println(CIPMUXcheck);
		delay(10);

    if ((readSerial().indexOf("ER")) == -1)
    {
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }

  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}

bool CGATT()
{
  Serial.println("inside CGATT");
  delay(10);
  String CGATTcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    Serial1.print("AT+CGATT\r");
    delay(500);
    CGATTcheck = readSerial();
    Serial.println(CGATTcheck);
    delay(10);

    if ((readSerial().indexOf("ER")) == -1)
    {
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }

  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}

bool CSTT()
{
  Serial.println("inside CSTT");
  delay(10);
  String CSTTcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    Serial1.print("AT+CSTT\r");
    delay(500);
    CSTTcheck = readSerial();
    Serial.println(CSTTcheck);
    delay(10);

    if ((readSerial().indexOf("ER")) == -1)
    {
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }

  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}

bool CIICR()
{
  Serial.println("inside CIICR");
  delay(10);
  String CIICRcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    Serial1.print("AT+CIICR\r");
    delay(500);
    CIICRcheck = readSerial();
    Serial.println(CIICRcheck);
    delay(10);

    if ((readSerial().indexOf("ER")) == -1)
    {
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }

  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}

bool CIFSR()
{
  Serial.println("inside CIFSR");
  delay(10);
  String CIFSRcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    Serial1.print("AT+CIFSR\r");
    delay(500);
    CIFSRcheck = readSerial();
    Serial.println(CIFSRcheck);
    delay(10);

    if ((readSerial().indexOf("ER")) == -1)
    {
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }
  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}

bool CIPSTART()
{
  Serial.println("inside CIPSTART");
  delay(10);
  String CIPSTARTcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    Serial1.print("AT+CIPSTART=\"TCP\",\"207.154.244.63\",\"80\"\r\n\r");
    delay(500);
    ATcheck = readSerial();
    Serial.println(ATcheck);
    delay(10);

    if ((readSerial().indexOf("ER")) == -1)
    {
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }

  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}

bool CIPSEND()
{
  Serial.println("inside CIPSEND");
  delay(10);
  String CIPSENDcheck;

  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    Serial1.print("AT+CIPSEND\r");
    delay(500);
    Serial1.print("GET /add-sensor-data/giriwar/pass12/1/1/1/");
    delay(5000);
    Serial1.print("000,000,000,000,000");
    delay(1000);
    Serial1.print("/00\r\n");
    delay(1000);
    Serial1.println(readSerial());
    Serial1.print(0x1a);
    delay(10000);
    Serial1.println(readSerial());

    if ((readSerial().indexOf("ER")) == -1)
    {
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }
  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}

const int pumpon_relay  =  40;
const int pumpoff_relay =  39;
const int solenoid1_relay =  38;
const int solenoid2_relay =  37;
const int solenoid3_relay =  36;


void setup()
{

  Serial.begin(9600);
  Serial.println("test serial");

  pinMode(pumpon_relay, OUTPUT);
  pinMode(pumpoff_relay, OUTPUT);
  pinMode(solenoid1_relay, OUTPUT);
  pinMode(solenoid2_relay, OUTPUT);
  pinMode(solenoid3_relay, OUTPUT);

  digitalWrite(pumpon_relay, LOW);
  digitalWrite(pumpoff_relay, LOW);
  digitalWrite(solenoid1_relay, LOW);
  digitalWrite(solenoid2_relay, LOW);
  digitalWrite(solenoid3_relay, LOW);
}


void loop()
{
  Serial.println("In loop");
  pumpOn();
  delay(1000);

  pumpOff();
  delay(1000);

  solenoid1Relay();
  delay(1000);

  solenoid2Relay();
  delay(1000);

  solenoid3Relay();
  delay(1000);
}

void pumpOn()
{
  Serial.println("in pumpON");
  digitalWrite(GREEN_LED, HIGH);
  
  digitalWrite(pumpon_relay, HIGH);
  delay(7000);
  digitalWrite(pumpon_relay, LOW);

  digitalWrite(GREEN_LED, LOW);
}


void pumpOff()
{
  Serial.println("in pumpOff");
  digitalWrite(RED_LED, HIGH);

  digitalWrite(pumpoff_relay, HIGH);
  delay(7000);
  digitalWrite(pumpoff_relay, LOW);

  digitalWrite(RED_LED, LOW);
}



void solenoid1Relay()
{
  Serial.println("in solenoid1 Relay");
  digitalWrite(BLUE_LED, HIGH);
  digitalWrite(RED_LED, HIGH);
  
  digitalWrite(solenoid1_relay, HIGH);
  delay(7000);
  digitalWrite(solenoid1_relay, LOW);

  digitalWrite(BLUE_LED, LOW);
  digitalWrite(RED_LED, LOW);
}



void solenoid2Relay()
{
  Serial.println("in solenoid2 Relay");
  digitalWrite(GREEN_LED, HIGH);
  digitalWrite(RED_LED, HIGH);
  
  digitalWrite(solenoid2_relay, HIGH);
  delay(7000);
  digitalWrite(solenoid2_relay, LOW);

  digitalWrite(GREEN_LED, LOW);
  digitalWrite(RED_LED, LOW);
}



void solenoid3Relay()
{
  Serial.println("in solenoid3 Relay");
  digitalWrite(BLUE_LED, HIGH);
  digitalWrite(GREEN_LED, HIGH);
  
  digitalWrite(solenoid3_relay, HIGH);
  delay(7000);
  digitalWrite(solenoid3_relay, LOW);

  digitalWrite(BLUE_LED, LOW);
  digitalWrite(GREEN_LED, LOW);
}


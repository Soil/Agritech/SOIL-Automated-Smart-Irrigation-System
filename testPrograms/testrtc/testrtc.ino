#include "time.h"
#define CEST 2*60*60

#include "RTC_Library.h"

// Prototypes
char * datestamp = __DATE__;
char * timestamp = __TIME__;

// Define variables and constants
time_t myEpochRTC;
tm myTimeRTC;

DateTime myRTC;


// Add setup code
void setup()
{
    Serial.begin(9600);
    delay(300);
    
    myRTC.begin();
    myRTC.setTimeZone(tz_CEST);
    
    Serial.print("*** datestamp = ");
    Serial.println(datestamp);
    Serial.print("*** timestamp = ");
    Serial.println(timestamp);
    
    // Fri, 31 Jul 2015 20:41:48 GMT
    myEpochRTC = 1438375308;
    
    // Set time to RTC, only once
    myRTC.setTime(myEpochRTC);
    Serial.print("Set RTC = ");
    Serial.println(myEpochRTC, DEC);
    
    myEpochRTC = 0;
}

// Add loop code
void loop()
{
    myEpochRTC = myRTC.getTime();
    Serial.print("Get RTC  = ");
    Serial.println(myEpochRTC);
    
    convertEpoch2Structure(myEpochRTC, myTimeRTC);
    
    Serial.print("RTC = \t");
    Serial.print(convertDateTime2String(myEpochRTC));
    Serial.print("\r");
    
    
    // Local time zone
    myEpochRTC = myRTC.getLocalTime();
    Serial.print("Get CEST = ");
    Serial.println(myEpochRTC);
    
    convertEpoch2Structure(myEpochRTC, myTimeRTC);
    
    Serial.print("CEST = \t");
    Serial.print(convertDateTime2String(myEpochRTC));
    Serial.print("\r");
    
    
    // PDT time zone
    myEpochRTC = myRTC.getTime();
    Serial.print("Get GMT = ");
    Serial.println(myEpochRTC);
    myEpochRTC  += tz_PDT;
    Serial.println(tz_PDT);
    Serial.print("Get PDT = ");
    Serial.println(myEpochRTC);
    
    convertEpoch2Structure(myEpochRTC, myTimeRTC);
    
    Serial.print("PDT = \t");
    Serial.print(convertDateTime2String(myEpochRTC));
    Serial.print("\r");
    
    // Even more flexible output!
    // see http://www.cplusplus.com/reference/ctime/strftime/
    Serial.println(formatDateTime2String("Now it's %I:%M %p.", myTimeRTC));
    
    Serial.println();
    
    delay(5000);
}

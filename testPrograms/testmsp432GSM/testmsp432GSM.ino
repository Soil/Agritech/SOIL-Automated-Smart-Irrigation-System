#define flushTime  15000
#define responseTime  15000
int numberOfTexts;
//long checkerForSMS = 10000;

char text;
bool textReceived;


void setup()
{
  Serial.println("in setup");
  startGSM(9600);
  delay(5000);
  
  Serial1.println("At+CMGF=1");
  delay(1000);


}


void loop()
{
  numberOfTexts = getNumSMS();
  Serial.print("numberOfTexts = ");
  Serial.print(numberOfTexts);
  textReceived = checkForStart(50);
  if (textReceived == true)
  {
    Serial.print("start");
  }
  else
  {
    Serial.println("control signal not found");
  }
  Serial.println(Serial1.readString());
}


void startGSM(int baud) {

  /*pinMode(GSM_RST,OUTPUT);
    digitalWrite(GSM_RST,HIGH);
    delay(100);
    digitalWrite(GSM_RST,LOW);
    delay(100);
    digitalWrite(GSM_RST,HIGH);
    delay(300); */
  Serial.begin(baud);
  Serial1.begin(baud);

  //send 3 times to ensure activation
  Serial1.println("AT\r");
  Serial1.println("AT\r");
  Serial1.println("AT\r");
  Serial1.println("ATE0\r"); //stop echos
  Serial.println("in startGSM");
}



bool checkForStart(int numberOfTexts)
{
  flushReceive(flushTime);
  Serial.println("in checkForStart");
  String number = String(numberOfTexts);
  String command = "AT+CMGR=" + number + "\r";
  Serial1.print(command);
  delay(1000);
  String ret = receiveToChar('$');
  if (ret.indexOf("start") != -1)
    return true;
  return false;
}


bool checkForLock(int numberOfTexts)
{
  flushReceive(flushTime);
  String number = String(numberOfTexts);
  String command = "AT+CMGR=" + number + "\r";
  Serial1.print(command);
  delay(1000);
  String ret = receiveToChar('$');
  if (ret.indexOf("LOCK") != -1)
    return true;
  return false;
}

void flushReceive(long time)
{
  Serial.println("in flushReceive");
  digitalWrite(GREEN_LED, HIGH);
  int time_start = millis();
  while (Serial1.available() > 0 || (millis() - time_start < time))
  {
    delay(10);
    if (Serial1.available() > 0)
    {
      char c = Serial1.read();
      if (c == '\r' || c == '\n') {
        continue;
      }
    }
  }
  digitalWrite(GREEN_LED, LOW);
}


String grabAllResponse(long time)
{
  Serial.println("in grabALLResponce");
  String readString = "";
  bool done = false;
  digitalWrite(RED_LED, HIGH);
  int time_start = millis();

  while (Serial1.available() > 0 || millis() - time_start < time) {
    delay(10);
    if (Serial1.available() > 0) {
      char c = Serial1.read();
      readString += c;
    }
  }
  digitalWrite(RED_LED, LOW);
  Serial.println(readString);
  return readString;
}


String receiveToChar(char in)
{
  Serial.println("in receiveToChar");
  String readString = "";
  bool done = false;
  while (Serial1.available() || !done) {
    delay(10);
    Serial.println("in receiveToChar while loop");
    char c = Serial1.read();
    if (c == in)
    {
      do {
        Serial.println("in receiveToChar if/do");
        digitalWrite(RED_LED, HIGH);
        if (c != in) {
          readString = readString + c;
        }
        c = Serial1.read();
      } while (c != in);
      digitalWrite(RED_LED, LOW);
      done = true;
    }
  }
  Serial.println(readString);
  return readString;
}


int getNumSMS()
{
  Serial.println("in getNumSMS");
  flushReceive(flushTime);
  Serial1.println("AT+CPMS=\"ME\"\r");
  String response = grabAllResponse(responseTime);
  return processNumSMSString(response);
}


int processNumSMSString(String response)
{
  Serial.println("in processNumSMSString");
  int index = response.indexOf(','); //first comma
  String temp = response.substring(index+1); //here to end
  int index2 = temp.indexOf(','); //next comma
  temp = temp.substring(0,index2); //grab number, do not include comma
  //grab integer from string
  int num = 0;
  for(int i = 0; i<temp.length(); i++)
  {
    char c = temp.charAt(i); //grab character
    if(i < temp.length() - 1)
    {
      //if there are more characters to read afterwords
      num = num + (c - '0'); //convert to integer
      num *= 10; //get ready for the next number
    }
    else
    {
      num += (c - '0');
    }
  }
  Serial.println("NumSMS = ");
  Serial.println(num);
  
  return num;
}




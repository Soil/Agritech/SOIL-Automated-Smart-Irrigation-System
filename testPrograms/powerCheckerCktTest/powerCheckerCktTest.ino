int redLed = 2;
int greenLed = 14;

int singlePhasePin = 9;
int threePhasePin = 10;



void setup()
{
  pinMode(redLed, OUTPUT);
  pinMode(greenLed, OUTPUT);

  pinMode(singlePhasePin, INPUT_PULLUP);
  pinMode(threePhasePin, INPUT_PULLUP);

  digitalWrite(redLed, LOW);
  digitalWrite(greenLed, LOW);
}


void loop()
{
  phaseTester();
}


void phaseTester()
{
  int singlePhaseStatus;
  int threePhaseStatus;

  singlePhaseStatus = digitalRead(singlePhasePin);
  threePhaseStatus = digitalRead(threePhasePin);

  if (singlePhaseStatus == 0) {

    digitalWrite(greenLed, LOW);
    digitalWrite(redLed, HIGH);
  }

  if (threePhaseStatus == 0) {

    digitalWrite(redLed, LOW);
    digitalWrite(greenLed, HIGH);
  }
}

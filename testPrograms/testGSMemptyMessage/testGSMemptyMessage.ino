int flushtime = 5000;
bool AT;
bool textModeSetup;
bool memoryModeSetup;
String allMessagesGrabed;
int timeout;
String buffer;
bool messageDeleted;
int pumpSwitch;
int pumpStatus;
int sensorMoist;



void setup()
{
  Serial.begin(9600);
  Serial1.begin(9600);
  //buffer.reserve(255);
  //
  buffer.reserve(255);
}

void loop()
{
  delay(100);
  Serial.println("test serial port");

  AT = ATinitiation();
  delay(500);
  textModeSetup = setTextmode();
  delay(500);
  memoryModeSetup = setMemoryType();
  delay(500);
  allMessagesGrabed = grabAllMessages();
  delay(500);
  messageDeleted = deleteMessage();
  
  Serial.println("AT =");
  delay(50);
  Serial.println(AT);
  delay(50);
  Serial.println("textModeSetup =");
  delay(50);
  Serial.println(textModeSetup);
  delay(50);
  Serial.println("memoryModeSetup =");
  delay(50);
  Serial.println(memoryModeSetup);
  delay(50);
  Serial.println("messageDeleted =");
  delay(50);
  Serial.println(messageDeleted);
  delay(50);
}



String readSerial()
{
  timeout = 0;

  while  (!Serial1.available() && timeout < 12000  )
  {
    delay(13);
    timeout++;
  }


  if (Serial1.available())
  {
    return Serial1.readString();
  }

}


void flushReceive(long time)
{
  digitalWrite(GREEN_LED, HIGH);
  int time_start = millis();
  while (Serial1.available() > 0 || (millis() - time_start < time))
  {
    delay(10);
    if (Serial1.available() > 0)
    {
      char c = Serial1.read();
      if (c == '\r' || c == '\n') {
        continue;
      }
    }
  }
  digitalWrite(GREEN_LED, LOW);
}


bool ATinitiation()
{
  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    Serial1.print("AT\r");
    delay(500);

    if ((readSerial().indexOf("ER")) == -1)
    {
      Serial.println("inside ATinitiation if");
      delay(500);

      return true;
    }

  }
  Serial.println("out of AT");
  delay(500);
  loop();
}


bool setTextmode()
{
  for (int k = 0; k < 6; k++)
  {
    Serial1.print("AT + CMGF = 1\r");
    delay(500);
    if ((readSerial().indexOf("ER")) == -1)
    {
      Serial.println("inside setTextmode if");
      delay(500);
      return true;
    }

  }
  //return false;
  Serial.println("error: out of setTextmode loop\r\n");
  delay(500);
  loop();
}


bool setMemoryType()
{
  for (int i = 0; i < 6; i++)
  {
    Serial1.print("AT + CPMS = \"SM\"\r");
    delay(500);
    String memType = readSerial();
    Serial.println(memType);
    delay(500);
    if ((memType.indexOf("ER")) == -1)
    {

      Serial.println("inside setMemoryType if");
      delay(500);
      return true;
    }
    //Serial.println("inside setMemoryType out of loop\r\n");
  }
  //return false;
  Serial.println("error: out of setMemoryType loop");
  delay(500);
  loop();
}



String grabAllMessages()
{

  for (int j = 0; j < 6; j++)
  {
    //delay(3000);
    flushReceive(flushtime);
    delay(1000);
    Serial1.print("AT + CMGR= 1\r");
    delay(300);
    buffer = readSerial();
    delay(1000);
    Serial.println(buffer);
     delay(3000);
    if ((buffer.indexOf("ER")) == -1)
    {
      //Serial.println("inside grabAllMessages in 1st if\r\n");
      
      if (buffer.indexOf("+CMGR:") != -1)
      {
        delay(500);
        //Serial.println(buffer);
        delay(3000);
        //delay(5000);
        Serial.println("grabAllMessages in 2nd if");
        delay(500);
        if (buffer.indexOf("$start") != -1)
        {
          Serial.println("found start");
          
          delay(50);
          return buffer;
        }

        else
        {
          Serial.println("no start found");
          return buffer;
        }

      }

    }
    //return "CMGR not found";
  }
  //return "error: unable to run AT+CMGR\r\n";
  Serial.println("error: out of grabAllMessages loop");
  delay(500);
  //loop();
  
  

}


bool deleteMessage()
{
  for (int i = 0; i < 6; i++)
  {
    Serial1.print("AT + CMGD = 1\r\n");
    String checkDelete = readSerial();
    Serial.println(checkDelete);
    if ((checkDelete.indexOf("ER")) == -1)
    {

      Serial.println("inside deleteMessage if");
      return true;
    }

  }
  Serial.println("error: out of deleteMessage loop");
  loop();
  //return false;
}



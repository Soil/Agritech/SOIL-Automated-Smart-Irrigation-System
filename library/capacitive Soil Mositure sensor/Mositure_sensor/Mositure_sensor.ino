/*---------------------------Program for Capacitive Soil Moisture Sensor-------------------------------------*/

const int analogInPin = A0;  // Analog input pin 

int sensorValue = 0;        // value read from the pot
unsigned int MS1 =0; 

void setup() 
{
  Serial.begin(9600); 
}

void loop()
{

  sensorValue = analogRead(analogInPin); 
  MS1=sensorValue;
  if(MS1<380){MS1=380;} 
  if(MS1>800){MS1=800;}
  
  MS1=(MS1-380)*10;
  MS1=100-(MS1/42);
             
  Serial.print("sensor = " );                       
  Serial.print(MS1);  
  Serial.print("\t output = ");      
  Serial.println(sensorValue);    
 
  delay(100);                     
}

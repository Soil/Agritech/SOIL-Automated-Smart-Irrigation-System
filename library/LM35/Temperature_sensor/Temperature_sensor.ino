/*---------------------------Program for Temperature Sensor-------------------------------------*/

const int analogInPin = A0;  // Analog input pin 

int sensorValue = 0;        // value read from the pot
float temp =0;
unsigned int TS1 =0; 

void setup() 
{
  Serial.begin(9600); 
}

void loop()
{

  sensorValue = analogRead(analogInPin); 
  temp = sensorValue;
  temp = (5.0 * temp * 100.0) / 1024; TS1= (temp-15)*10;  
  
  Serial.print("sensor = " );                       
  Serial.print(TS1);  
  Serial.print("\t output = ");      
  Serial.println(sensorValue);    
 
  delay(100);                     
}

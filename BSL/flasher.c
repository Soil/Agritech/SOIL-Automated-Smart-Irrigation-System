/*====================================================================================================*/
	/* Program to access the RTS,of a Serial Port(ttyS* or ttyUSB0) using ioctl() system call.            */
	/*                                                                                                    */
	/*====================================================================================================*/
	/*                                                                                                    */
	/* If your are using FT232 based USB to Serial Converter all the pins are inverted internally         */
	/* Modem Pins of FT232 based Serial Port -> ~RTS,,~DTR.                                               */
	/* SETTING the Pins will make it LOW,while CLEARING the Pins will make it HIGH.                       */ 
	/*----------------------------------------------------------------------------------------------------*/

	/*----------------------------------------------------------------------------------------------------*/
	/* Compiler/IDE  : gcc 4.6.3                                                                          */
	/* Library       :                                                                                    */
	/* Commands      : gcc -o rts rts.c                                                                   */
	/* OS            : Linux(x86) (Linux Mint 13 Maya/Ubuntu 14.04)(Linux Kernel 3.x.x)                  */                              
	/* Programmer    : Rahul.S                                                                            */
	/* Date	         : 27-December-2014                                                                   */
	/*====================================================================================================*/

	/*====================================================================================================*/
	/* Running the executable                                                                             */
	/* ---------------------------------------------------------------------------------------------------*/ 
	/* 1) Compile the  serialport_read.c  file using gcc on the terminal (without quotes)                 */
        /*                                                                                                    */
	/*	" gcc -o rts rts.c  "                                                                         */
	/*                                                                                                    */
        /* 2) Linux will not allow you to access the serial port from user space,you have to be root.So use   */
        /*    "sudo" command to execute the compiled binary as super user.                                    */
        /*                                                                                                    */
        /*       "sudo ./rts"                                                                   */
	/*                                                                                                    */
	/*====================================================================================================*/

	/*====================================================================================================*/
	/* Sellecting the Serial port Number on Linux                                                         */
	/* ---------------------------------------------------------------------------------------------------*/ 
	/* /dev/ttyUSBx - when using USB to Serial Converter, where x can be 0,1,2...etc                      */
	/* /dev/ttySx   - for PC hardware based Serial ports, where x can be 0,1,2...etc                      */
        /*====================================================================================================*/
	
	/*==================================*/
	/*       MODEM bits                 */
	/*------------------- --------------*/
	/*   TIOCM_DTR --- DTR Line         */
	/*   TIOCM_RTS --- RTS Line         */
	/*==================================*/

#include <stdio.h>
#include <fcntl.h>   	 /* File Control Definitions           */
#include <termios.h>	 /* POSIX Terminal Control Definitions */
#include <unistd.h> 	 /* UNIX Standard Definitions 	       */
#include <errno.h>   	 /* ERROR Number Definitions           */
#include <sys/ioctl.h> /* ioctl()                          */
#include <stdlib.h>
#include <time.h>
//#include <sys/types.h>
//#include <signal.h>
#include <unistd.h>


/*
#define NSEC_PER_SEC 1000000000L

#define MAX_TESTS    10000

#define timerdiff(a,b) (((a)->tv_sec - (b)->tv_sec) * NSEC_PER_SEC + \
(((a)->tv_nsec - (b)->tv_nsec)))


static struct timespec prev = {.tv_sec=0,.tv_nsec=0};
static int count = MAX_TESTS;
unsigned long diff_times[MAX_TESTS];


void delay(int millisec) 
{
	clock_gettime(CLOCK_MONOTONIC)

	int i = 0;
    timer_t t_id;

    struct itimerspec tim_spec = {.it_interval= {.tv_sec=0,.tv_nsec=500000},
                    .it_value = {.tv_sec=1,.tv_nsec=0}};

    struct sigaction act;
    sigset_t set;

    sigemptyset( &set );
    sigaddset( &set, SIGALRM );

    act.sa_flags = 0;
    act.sa_mask = set;
    act.sa_handler = &handler;

    sigaction(SIGALRM, &act, NULL);

    if (timer_create(CLOCK_MONOTONIC, NULL, &t_id))
        perror("timer_create");

    clock_gettime(CLOCK_MONOTONIC, &prev);

    if (timer_settime(t_id, 0, &tim_spec, NULL))
        perror("timer_settime");

    while(1);

    return 0;
}
*/

/*
static const struct timespec{
	time_t tv_sec;        // seconds 
    long   tv_nsec;       // nanoseconds 
} ts;
*/

struct timespec ts;

void sleep_ms(int milliseconds)
{
    ts.tv_sec = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;
    nanosleep(&ts, NULL);
}


void sleep_us(int microseconds)
{
    ts.tv_sec = microseconds / 1000000;
    ts.tv_nsec = (microseconds % 1000000) * 1000;
    nanosleep(&ts, NULL);
}

void sleep_ns(int long nanoseconds)
{
  ts.tv_sec = nanoseconds / 1000000;
  ts.tv_nsec = nanoseconds;
  nanosleep(&ts, NULL);
}


void main(void)
{
	int fd;	/*File Descriptor*/


	/*------------------ Opening the Serial port ------------------*/
	/* Change /dev/ttyUSB0 to the one corresponding to your system */
	/* O_RDWR Read/Write access to serial port           */
	/* O_NOCTTY - No terminal will control the process   */
	/* Blocking Mode  */
    fd = open("/dev/ttyUSB0",O_RDWR | O_NOCTTY );

	/* Error Checking */
    if(fd == -1) {
    	printf("\n    Error! in Opening ttyUSB0  ");
    }
    else {
    	//printf("\n    ttyUSB0 Opened Successfully \n");
	}


	/*--------- Controlling the RTS and DTR pins of CP2102 to generate BSL invoke sequence --------*/
	int RTS_flag, DTR_flag;

	RTS_flag = TIOCM_RTS;	/* Modem Constant for RTS pin */
	DTR_flag = TIOCM_DTR; /* Modem Constant for DTR pin */

  /*
	// sequence 1
	ioctl(fd, TIOCMBIC, &RTS_flag); // TIOCMBIC - Clear the bit corrosponding to  RTS_flag
	ioctl(fd, TIOCMBIC, &DTR_flag); // TIOCMBIS - Set the bit corrosponding to  DTR_flag
	sleep_ms(3000);

	ioctl(fd,TIOCMBIS,&RTS_flag); // TIOCMBIC - Clear the bit corrosponding to  RTS_flag
	sleep_us(10);
	ioctl(fd,TIOCMBIC,&RTS_flag); // fd -file descriptor pointing to the opened Serial port
										// TIOCMBIS - Set the bit corrosponding to  RTS_flag
	sleep_us(50);
	ioctl(fd,TIOCMBIS,&RTS_flag);	// TIOCMBIC - Clear the bit corrosponding to  RTS_flag
	sleep_us(10);
	ioctl(fd,TIOCMBIS,&DTR_flag); // setting DTR = 0,~DTR = 1
	sleep_us(10);
	ioctl(fd,TIOCMBIC,&RTS_flag);
  */

	ioctl(fd, TIOCMBIS, &RTS_flag);
	ioctl(fd, TIOCMBIC, &DTR_flag);
	sleep_ms(3000);

	ioctl(fd, TIOCMBIS, &DTR_flag);
	sleep_ns(500);
	ioctl(fd, TIOCMBIC, &RTS_flag);
	sleep_ns(500);
	ioctl(fd, TIOCMBIS, &RTS_flag);
	sleep_ns(500);
	ioctl(fd, TIOCMBIC, &RTS_flag);
	sleep_ns(500);
	ioctl(fd, TIOCMBIC, &DTR_flag);
	sleep_ns(500);
	ioctl(fd, TIOCMBIS, &RTS_flag);

	sleep_ms(2000);

	close(fd); /* Close the Opened Serial Port */
}

const int pumpon_relay  =  39;
const int pumpoff_relay =  38;
int flushtime = 5000;
bool AT;
bool textModeSetup;
bool memoryModeSetup;
String allMessagesGrabed;
int timeout;
String buffer;
String timerBuffer;
String moistBuffer;
bool messageDeleted;
int pumpSwitch;
int pumpStatus;
int sensorMoist;
int modeIndex;
char MyByte[20];
int globCounter = 0;




void setup()
{
  Serial.begin(9600);
  Serial1.begin(9600);
  //buffer.reserve(255);
  //
  pinMode(pumpon_relay, OUTPUT);
  pinMode(pumpoff_relay, OUTPUT);

  digitalWrite(pumpon_relay, LOW);
  digitalWrite(pumpoff_relay, LOW);

  buffer.reserve(255);
  smsAckSend("Device switched ON", "+918349504911");
}


void loop()
{
  delay(100);
  //Serial.println("test serial port");

  AT = ATinitiation();
  delay(500);
  textModeSetup = setTextmode();
  delay(500);
  memoryModeSetup = setMemoryType();
  delay(500);
  allMessagesGrabed = grabAllMessages();
  delay(500);
  //messageDeleted = deleteMessage();
  //delay(500);
  modeSelector();
  delay(100);
  sensorRead();
  delay(100);

  
  GPRSserverUpdate();
  delay(100);

  /*
    Serial.println("AT =");
    delay(50);
    Serial.println(AT);
    delay(50);
    Serial.println("textModeSetup =");
    delay(50);
    Serial.println(textModeSetup);
    delay(50);
    Serial.println("memoryModeSetup =");
    delay(50);
    Serial.println(memoryModeSetup);
    delay(50);
    Serial.println("messageDeleted =");
    delay(50);
    Serial.println(messageDeleted);
    delay(50);

  */
  //Serial.println(messageDeleted);
  //Serial.println(allMessagesGrabed);
  //delay(3000);
  // if (AT & textModeSetup & memoryModeSetup & allMessagesGrabed);
  // {
  //  Serial.println("success!");
  // }



}


String readSerial()
{
  timeout = 0;

  while  (!Serial1.available() && timeout < 12000  )
  {
    delay(13);
    timeout++;
  }


  if (Serial1.available())
  {
    return Serial1.readString();
  }

}


void flushReceive(long time)
{
  digitalWrite(GREEN_LED, HIGH);
  int time_start = millis();
  while (Serial1.available() > 0 || (millis() - time_start < time))
  {
    delay(10);
    if (Serial1.available() > 0)
    {
      char c = Serial1.read();
      if (c == '\r' || c == '\n')
      {
        continue;
      }
    }
  }
  digitalWrite(GREEN_LED, LOW);
}


bool ATinitiation()
{
  for (int l = 0; l < 6; l++)
  {
    //delay(500);
    Serial1.print("AT\r");
    delay(500);

    if ((readSerial().indexOf("ER")) == -1)
    {
      //Serial.println("inside ATinitiation if");
      //delay(500);

      return true;
    }

  }
  //Serial.println("out of AT");
  //delay(500);
  loop();
}


bool setTextmode()
{
  for (int k = 0; k < 6; k++)
  {
    Serial1.print("AT + CMGF = 1\r");
    delay(500);
    if ((readSerial().indexOf("ER")) == -1)
    {
      //Serial.println("inside setTextmode if");
      //delay(500);
      return true;
    }

  }
  //return false;
  //Serial.println("error: out of setTextmode loop\r\n");
  //delay(500);
  loop();
}


bool setMemoryType()
{
  for (int i = 0; i < 6; i++)
  {
    Serial1.print("AT + CPMS = \"SM\"\r");
    delay(500);
    String memType = readSerial();
    //Serial.println(memType);
    //delay(500);
    if ((memType.indexOf("ER")) == -1)
    {

      //Serial.println("inside setMemoryType if");
      //delay(500);
      return true;
    }
    //Serial.println("inside setMemoryType out of loop\r\n");
  }
  //return false;
  //Serial.println("error: out of setMemoryType loop");
  //delay(500);
  loop();
}



String grabAllMessages()
{

  for (int j = 0; j < 6; j++)
  {
    //delay(3000);
    flushReceive(flushtime);
    delay(1000);
    Serial1.print("AT + CMGR= 1\r");
    delay(4000);
    buffer = readSerial();
    delay(2000);
    //Serial.println(buffer);
    //delay(1000);

    if ((buffer.indexOf("ER")) == -1)
    {
      //Serial.println("inside grabAllMessages in 1st if\r\n");

      if (buffer.indexOf("+CMGR:") != -1)
      {


        //delay(500);
        //delay(5000);
        //Serial.println("grabAllMessages in 2nd if");
        //delay(500);
        if (buffer.indexOf("$stop") != -1)
        {
          //Serial.println("found stop");
          //delay(10);
          modeIndex = 0;
          //Serial.println("modeIndex =");
          //delay(10);
          //Serial.println(modeIndex);
          //delay(10);
          modeSelector();
          deleteMessage();

          delay(10);
          return buffer;
        }


        if (buffer.indexOf("$start") != -1)
        {
          //Serial.println("found start");
          //delay(10);
          modeIndex = 1;
          //Serial.println("modeIndex =");
          //delay(10);
          //Serial.println(modeIndex);
          //delay(10);

          //parsePhoneNumber(buffer);
          modeSelector();
          deleteMessage();
          delay(10);
          return buffer;
        }



        if (buffer.indexOf("#") != -1)
        {
          //Serial.println("found timer mode");
          //delay(10);
          modeIndex = 2;
          //Serial.println("modeIndex =");
          //delay(10);
          //Serial.println(modeIndex);
          //delay(10);
          timerBuffer = buffer;
          delay(500);
          //Serial.println("timerBuffer =");
          //delay(10);
          //Serial.println(timerBuffer);
          //delay(100);
          String smsTimerMessage = "Timer mode ON";
          smsAckSend(smsTimerMessage, timerBuffer);
          delay(10);
          modeSelector();
          deleteMessage();
          delay(10);
          return buffer;
        }

        if (buffer.indexOf("*") != -1)
        {
          //Serial.println("found moisture mode");
          //delay(10);
          modeIndex = 3;
          //Serial.println("modeIndex =");
          //delay(10);
          //Serial.println(modeIndex);
          //delay(10);
          moistBuffer = buffer;
          delay(500);
          //Serial.println("moistBuffer =");
          //delay(10);
          //Serial.println(moistBuffer);
          //delay(100);
          deleteMessage();
          String smsMoistMessage = "Moisture mode ON";
          smsAckSend(smsMoistMessage, moistBuffer);
          //delay(10);

          modeSelector();
          deleteMessage();
          delay(10);
          return buffer;
        }
        else
        {
          //Serial.println("not found any message");
          //delay(500);
          deleteMessage();
          return buffer;
        }

      }

    }
    else
    {
      deleteMessage();
    }
    return buffer;
  }
  //return "error: unable to run AT+CMGR\r\n";
  //Serial.println("error: out of grabAllMessages loop");
  //delay(500);
  //loop();
  return buffer;

}


void modeSelector()
{
  switch (modeIndex)
  {
    case 0:
      delay(10);
      //Serial.println("In modeIndex = 0");
      //delay(10);
      manualModePumpStop();
      delay(100);
      break;

    case 1:
      delay(10);
      //Serial.println("In modeIndex = 1");
      //delay(10);
      manualModePumpStart();
      delay(100);
      break;

    case 2:
      delay(10);
      //Serial.println("In modeIndex = 2");
      //delay(10);
      timerMode();
      delay(100);
      break;

    case 3:
      delay(10);
      //Serial.println("In modeIndex = 3");
      //delay(10);
      moistureMode();
      delay(100);
      break;

  }
}


void manualModePumpStart()
{
  pumpSwitch = 1;
  pump();
  //pumpSwitch = 0;

}

void manualModePumpStop()
{
  pumpSwitch = 0;
  pump();

}



void timerMode()
{
  String pattern = "#";
  int patternIndex;
  char charBuffer[150];
  char charpattern[20];
  char message[9];
  int startHours;
  int startMinutes;
  int stopHours;
  int stopMinutes;
  //int intMessage;
  int *pointerTimeDigits;
  int timeDigits[9];
  String RTCtime;
  char charRTCtime[14];
  char *pointerCurrentTime;

  //Serial.println("test timer mode");
  //delay(500);
  //Serial.println(line[8]);
  //delay(500);
  timerBuffer.toCharArray(charBuffer, 150);
  delay(500);
  pattern.toCharArray(charpattern, 20);
  delay(500);
  //Serial.println("charBuffer =");
  //delay(50);
  //Serial.println(charBuffer);
  //delay(100);



  int h, l = 0;
  patternIndex = stringIndexer(charBuffer, charpattern);
  //Serial.println("patternIndex =");
  //delay(50);
  //Serial.println(patternIndex);
  for (h = patternIndex + 1; l < 9; h++, l++)
  {
    //Serial.println(l);
    //Serial.println(h);
    //Serial.println("inside loop");
    message[l] = charBuffer[h];
  }
  //Serial.println("message =");
  //delay(50);
  //Serial.println(message);
  //delay(100);
  pointerTimeDigits = messageFormatter(message, l - 2);

  for (int i = 0; i < l; i++)
  {
    timeDigits[i] = *(pointerTimeDigits + i);
    //Serial.println("i =");
    //delay(10);
    //Serial.println(i);
    //delay(10);
    //Serial.println("timeDigits");
    //delay(10);
    //Serial.println(timeDigits[i]);
  }
  //Serial.println("timeDigits =");
  //delay(50);
  //Serial.println(timeDigits);

  startHours = ((timeDigits[0] * 10) + timeDigits[1]);
  //Serial.println("startHours =");
  //delay(10);
  //Serial.println(startHours);
  //delay(100);
  startMinutes = ((timeDigits[2] * 10) + timeDigits[3]);
  //Serial.println("startMinutes =");
  //delay(10);
  //Serial.println(startMinutes);
  //delay(100);
  stopHours = ((timeDigits[4] * 10) + timeDigits[5]);
  //Serial.println("stopHours =");
  //delay(10);
  //Serial.println(stopHours);
  //delay(100);
  stopMinutes = ((timeDigits[6] * 10) + timeDigits[7]);
  //Serial.println("stopMinutes =");
  //delay(10);
  //Serial.println(stopMinutes);
  //delay(100);

  pointerCurrentTime = gsmRTC();

  for (int x = 0; x < 14; x++)
  {
    charRTCtime[x] = *(pointerCurrentTime + x);
  }


  char charRTCyear[2];
  char charRTCmonth[2];
  char charRTCdate[2];
  char charRTChours[2];
  char charRTCminutes[2];

  int z = 0;
  for (int y = 0; y < 2 && z < 2; y++, z++)
  {
    charRTCyear[y] = charRTCtime[z];
  }

  z++; //to skip symbols in between the numbers
  for (int u = 0; u < 2 && z < 5; u++, z++)
  {
    charRTCmonth[u] = charRTCtime[z];
  }

  z++;
  for (int q = 0; q < 2 && z < 8; q++, z++)
  {
    charRTCdate[q] = charRTCtime[z];
  }

  z++;
  for (int w = 0; w < 2 && z < 11; w++, z++)
  {
    charRTChours[w] = charRTCtime[z];
  }

  z++;
  for (int e = 0; e < 2 && z < 14; e++, z++)
  {
    charRTCminutes[e] = charRTCtime[z];
  }

  int intRTCyear;
  int intRTCmonth;
  int intRTCdate;
  int intRTChours;
  int intRTCminutes;

  intRTCyear = atol(charRTCyear);
  intRTCmonth = atol(charRTCmonth);
  intRTCdate = atol(charRTCdate);
  intRTChours = atol(charRTChours);
  intRTCminutes = atol(charRTCminutes);

  //delay(10);
  //Serial.println("intRTCyear =");
  //delay(10);
  //Serial.println(intRTCyear);
  //delay(10);
  //Serial.println("intRTCmonth =");
  //delay(10);
  //Serial.println(intRTCmonth);
  //delay(10);
  //Serial.println("intRTCdate =");
  //delay(10);
  //Serial.println(intRTCdate);
  //delay(10);
  //Serial.println("intRTChours =");
  //delay(10);
  //Serial.println(intRTChours);
  //delay(10);
  //Serial.println("intRTCminutes");
  //delay(10);
  //Serial.println(intRTCminutes);
  //delay(10);


  //smsAckSend("Timer mode ON");
  timerComparision(startHours, startMinutes, stopHours, stopMinutes, intRTChours, intRTCminutes, intRTCdate, intRTCmonth, intRTCyear);

}


void moistureMode()
{
  String pattern = "*";
  int patternIndex;
  char charBuffer[150];
  char charpattern[20];
  char message[5];
  int moistLow;
  int moistHigh;
  int *pointerMoistDigits;
  int moistDigits[4];


  //Serial.println("test moisture mode");
  //delay(500);
  //Serial.println(line[8]);
  //delay(500);
  moistBuffer.toCharArray(charBuffer, 150);
  delay(500);
  pattern.toCharArray(charpattern, 20);
  delay(500);
  //Serial.println(charBuffer);
  //delay(100);

  String strSensorMoist;
  char charSensorMoist[30];

  char strMyByte[22];
  strMyByte[21] = '\0';
  for (int a = 0; a < 21; a++)
  {
    //Serial.println("a =");
    //delay(10);
    //Serial.println(a);
    //delay(10);
    //Serial.println("MyByte=");
    //delay(10);
    //Serial.println(MyByte);
    //delay(10);
    //Serial.println("strMyByte =");
    //delay(10);
    //Serial.println(strMyByte);
    //delay(10);
    strMyByte[a] = MyByte[a];
    delay(10);
  }

  delay(100);


  //Serial.println(globCounter);
  globCounter++;

  if (globCounter > 25)
  {
    //Serial.println("inside str message loop");
    smsAckSend(strMyByte, "+918349504911");

  }


  if (globCounter >= 26)
  {
    //Serial.println("repeating globCounter");
    globCounter = 0;
  }





    for (int b = 0; b < 21; b++)
    {
      charSensorMoist[b] = MyByte[b];
    }
  //charSensorMoist = MyByte;
  //strSensorMoist = String(charSensorMoist)
  int r = 1;
  char charSensor1[2];
  for (int p = 0; p < 2 && r < 3; p++, r++)
  {

    charSensor1[p] = charSensorMoist[r];
  }
  //delay(10);
  //Serial.println("charSensor1 =");
  //delay(10);
  //Serial.println(charSensor1);
  //delay(10);

  //String strSensor1;
  //strSensor1 = String(charSensor1);
  int intSensor1;

  if (charSensor1[0] != 'e' && charSensor1[1] != 'e')
  {
    intSensor1 = atol(charSensor1);
    delay(10);
    //Serial.println("intSensor1 =");
    //delay(10);
    //Serial.println(intSensor1);
    //delay(10);
  }

  else
  {
    //Serial.println("eee eee in sensorRead");
    //delay(10);
    return;
  }


  r = 5;
  char charSensor2[2];
  for (int t = 0; t < 2 && r < 7; t++, r++)
  {

    charSensor2[t] = charSensorMoist[r];
  }

  //delay(10);
  //Serial.println("charSensor2 =");
  //delay(10);
  //Serial.println(charSensor2);
  //delay(10);

  //String strSensor2;
  //strSensor2 = String(charSensor2);
  int intSensor2;

  if (charSensor2[0] != 'e' && charSensor2[1] != 'e')
  {
    intSensor2 = atol(charSensor2);
    //delay(10);
    //Serial.println("intSensor2 =");
    //delay(10);
    //Serial.println(intSensor2);
    //delay(10);
  }

  else
  {
    //Serial.println("eee eee in sensorRead");
    //delay(10);
    return;
  }


  sensorMoist = intSensor2; //save the sensor data to be compared

  int h, l = 0;
  patternIndex = stringIndexer(charBuffer, charpattern);

  //Serial.println(patternIndex);
  for (h = patternIndex + 1; l < 4; h++, l++)
  {
    //Serial.println(l);
    //Serial.println(h);
    //Serial.println("inside loop");
    message[l] = charBuffer[h];
  }
  //Serial.println(message);
  //delay(100);

  pointerMoistDigits = messageFormatter(message, l);

  for (int i = 0; i < l; i++)
  {
    moistDigits[i] = *(pointerMoistDigits + i + 1);
  }

  moistLow = ((moistDigits[0] * 10) + moistDigits[1]);
  //Serial.println(moistLow);
  //delay(100);
  moistHigh = ((moistDigits[2] * 10) + moistDigits[3]);
  //Serial.println(moistHigh);
  //delay(100);

  if (sensorMoist != 0)
  {

    if (sensorMoist > moistHigh)
    {
      //Serial.println("moisture high need to stop pump");
      delay(10);
      if (pumpStatus == 1)
      {
        pumpSwitch = 0;
        pump();
      }
    }

    if (sensorMoist < moistLow)
    {
      //Serial.println("moisture low need to start pump");
      delay(10);
      if (pumpStatus == 0)
      {
        pumpSwitch = 1;
        pump();
      }

    }
  }
  else
  {
    //Serial.println("error: sensorMoist = 0");
  }

}




int stringIndexer(char source[], char sign[])
{
  int i, j, k, result;

  result = -1;

  for (i = 0; source[i] != '\0'; i++)
  {
    for (j = i, k = 0; sign[k] != '\0' && source[j] == sign[k]; j++, k++)
      ;
    //Serial.println(k);
    //delay(100);
    if (k > 0 && sign[k] == '\0')
    {
      //Serial.println("inside if");
      //delay(100);
      result = i;
      //Serial.println("interimResult =" + result );
    }
  }
  return result;

}


int * messageFormatter(char charMessage[], int maxIndex)
{
  unsigned int intMessage;
  static int dig[12];
  //String strMessage;

  //strMessage = String(charMessage);
  intMessage = atol(charMessage);

  int j = maxIndex;
  //int j = 7;

  for (int i = j; i > -1 ; i--)
  {
    dig[i] = intMessage % 10;
    //Serial.println("j =");
    //delay(10);
    //Serial.println(i);
    //delay(10);
    //Serial.println("dig =");
    //delay(10);
    //Serial.println(dig[i]);
    //delay(10);
    intMessage = intMessage / 10;
  }
  return dig;
}

char * gsmRTC()
{
  //char charCurrentTime[100];
  String cclkOut;
  static char currentTime[14];

  Serial1.print("AT+CCLK?\r");
  delay(500);
  cclkOut = readSerial();
  delay(500);

  if (cclkOut.indexOf("ER") == -1)
  {
    //Serial.println("no ERR in gsmRTC");
    int h = 19;
    for (int j = 0 ; h < 33 && j < 14; j++, h++)
    {
      currentTime[j] = cclkOut[h];
    }

    //Serial.println("currentTime =");
    //delay(10);
    //Serial.println(currentTime);
    //delay(100);

  }

  else
  {
    //Serial.println("ERR in gsmRTC");
    //delay(100);
  }

  return currentTime;
}



void timerComparision(int startHours, int startMinutes, int stopHours, int stopMinutes, int intRTChours, int intRTCminutes, int intRTCdate, int intRTCmonth, int intRTCyear)
{
  if (intRTChours >= startHours && intRTChours <= stopHours)
  {
    //Serial.println("in 1st if");
    if (intRTChours == startHours)
    {
      if (intRTCminutes >= startMinutes)
      {
        if (intRTChours == stopHours)
        {
          if (intRTCminutes <= stopMinutes)
          {
            //Serial.println("in 2nd if");
            //pumpStatus = 1;
            pumpSwitch = 1;
            pump();
          }
          else if (intRTCminutes > stopMinutes)
          {
            //Serial.println("in 2nd if");
            //pumpStatus = 1;
            pumpSwitch = 0;
            pump();
          }
        }

      }
      else
      {
        pumpSwitch = 0;
        //pumpStatus = 0;
        pump();
      }



    }

    if (intRTChours > startHours)
    {
      if (intRTChours == stopHours)
      {
        if (intRTCminutes <= stopMinutes)
        {
          //Serial.println("in 2nd if");
          //pumpStatus = 1;
          pumpSwitch = 1;
          pump();
        }

        else
        {
          //pumpStatus = 0;
          pumpSwitch = 0;
          pump();
        }
      }


      if (intRTChours < stopHours)
      {
        //pumpStatus = 1;
        pumpSwitch = 1;
        pump();
      }

    }

  }

  else
  {
    //Serial.println("in else");
    pumpSwitch = 0;
    //pumpStatus = 0;
    pump();
  }
}



void sensorRead()
{
  unsigned int i, N, count;
  char ch;
  i = 0; N = 19;
  count = 8000;
  Serial.print("D");
  while (i < N && count > 0)
  {
    if (Serial.available())
    {
      ch = Serial.read();
      MyByte[i] = ch;
      i++;
      count = 8000;
    }
    else {
      delay(1);
      count--;
    }
  }

  if (i == 19)
  {
    for (i = 0; i < 19; i++) {
      //Serial.print( MyByte[i]);
      delay(10);
    }
  }
  else
  {
    MyByte[0] = 'e';
    MyByte[1] = 'e';
    MyByte[2] = 'e';
    MyByte[3] = ',';
    MyByte[4] = 'e';
    MyByte[5] = 'e';
    MyByte[6] = 'e';
    MyByte[7] = ',';
    MyByte[8] = 'e';
    MyByte[9] = 'e';
    MyByte[10] = 'e';
    MyByte[11] = ',';
    MyByte[12] = 'e';
    MyByte[13] = 'e';
    MyByte[14] = 'e';
    MyByte[15] = ',';
    MyByte[16] = 'e';
    MyByte[17] = 'e';
    MyByte[18] = 'e';
    delay(10);
    //Serial.print( MyByte[i]);
    //Serial.println("MyByte =");
    //Serial.println(MyByte);
    //delay(10);
    //Serial.println("eee,eee,eee,eee,eee");
    //delay(10);

  }
}



bool deleteMessage()
{
  //Serial.println("in deleteMessage");
  //delay(50);
  for (int i = 0; i < 6; i++)
  {
    Serial1.print("AT + CMGD = 1\r\n");
    delay(500);
    String checkDelete = readSerial();
    delay(500);
    //Serial.println(checkDelete);
    //delay(50);


    if ((checkDelete.indexOf("ER")) == -1)
    {

      //Serial.println("inside deleteMessage if");
      return true;
    }

  }
  //Serial.println("error: out of deleteMessage loop");
  loop();
  //return false;
}


void GPRSserverUpdate()
{
  //Serial.println("in GPRSserverUpdate");
  delay(10000);

  Serial1.print("AT\r\n");
  delay(4000);

  Serial1.print("AT+CIPSHUT\r\n");
  delay(4000);

  Serial1.print("AT+CIPMUX=0\r\n");
  delay(4000);

  Serial1.print("AT+CGATT=1\r\n");
  delay(4000);

  Serial1.print("AT+CSTT=\"www\",\"\",\"\"\r\n");
  delay(4000);

  Serial1.print("AT+CIICR\r\n");
  delay(8000);

  Serial1.print("AT+CIFSR\r\n");
  delay(4000);

  Serial1.print("AT+CIPSTART=\"TCP\",\"207.154.244.63\",\"80\"\r\n");
  delay(15000);

  Serial1.print("AT+CIPSEND=66\r\n");
  delay(4000);

  Serial1.print("GET /add-sensor-data/giriwar/pass12/1/1/1/");
  delay(5000);

  Serial1.print(MyByte);
  delay(9000);

  Serial1.print("/00\r\n");
  delay(1000);

  Serial1.print(0x1a);
  delay(10000);

  return;

}




void smsAckSend(String acknowledgment, String phoneNoBuffer)
{
  //Serial.println("in smsAckSend");
  delay(10);
  digitalWrite(RED_LED, HIGH);
  delay(1000);
  digitalWrite(RED_LED, LOW);
  delay(100);
  digitalWrite(BLUE_LED, HIGH);
  delay(1000);
  digitalWrite(BLUE_LED, LOW);
  delay(100);
  digitalWrite(GREEN_LED, HIGH);
  delay(1000);
  digitalWrite(GREEN_LED, LOW);
  //delay(10);
  //Serial.println(phoneNoBuffer);
  delay(10);
  char charPhoneNo[16];
  //int j = phoneNoBuffer.indexOf("+");
  int j;
  //Serial.println("index of + =");
  //delay(10);
  //Serial.println(j);
  //delay(10);
  //Serial.println("message to send =");
  //Serial.println(acknowledgment);


  /*

    for (int v = 0; v < 80; v++)
    {
    Serial.println("v =");
    delay(10);
    Serial.println(v);
    delay(10);
    Serial.println(phoneNoBuffer[v]);
    delay(10);
    if (phoneNoBuffer[v] == '+')
    {
      Serial.println("Index of + =");
      delay(50);
      Serial.println(v);
      delay(50);
      if ((v > 34 && v < 56) || v == 0)
      {
        j = v;
      }
    }
    }

  */

  for (int i = 0; i < 14 && j < 100; i++, j++)
  {
    //Serial.println("inside loop");
    //delay(100);
    //Serial.println("i =");
    //delay(100);
    //Serial.println(i);
    //delay(100);
    //Serial.println("j =");
    //delay(100);
    //Serial.println(j);
    //delay(100);

    charPhoneNo[i] = phoneNoBuffer[j];
    //Serial.println(phoneNo[i]);
    //Serial.println(phoneNoBuffer[j]);
  }
  //delay(100);
  //Serial.println(phoneNo);

  String strPhoneNo;
  strPhoneNo = String(charPhoneNo);
  delay(100);
  //Serial.println("phoneNoBuffer =");
  //delay(100);
  //Serial.println(phoneNoBuffer);
  //delay(100);
  //Serial.println("string phone no =");
  //delay(100);
  //Serial.println(strPhoneNo);
  //delay(100);
  //Serial.println("message to send =");
  //delay(100);
  //Serial.println(acknowledgment);
  //delay(100);


  Serial1.print("AT+CMGS=\"");
  delay(1000);
  Serial1.print("+918349504911");
  delay(1000);
  Serial1.println("\"\r");
  delay(1000);
  Serial1.println(acknowledgment);
  delay(100);
  Serial1.println((char)26);
  delay(100);


}


void pump()
{
  //Serial.println("in pump function");
  //delay(50);
  if (pumpSwitch == 1 && pumpStatus == 0)
  {
    digitalWrite(BLUE_LED, HIGH);

    //Serial.println("pump ON");
    //delay(100);
    digitalWrite(pumpon_relay, HIGH);
    delay(2500);
    digitalWrite(pumpon_relay, LOW);
    pumpStatus = 1;
    String smsPumpMessage = "Pump ON";
    smsAckSend(smsPumpMessage, buffer);
    delay(10);
    digitalWrite(BLUE_LED, LOW);
  }

  if (pumpSwitch == 0 && pumpStatus == 1)
  {
    digitalWrite(RED_LED, HIGH);
    //Serial.println("pump OFF");
    //delay(100);
    digitalWrite(pumpoff_relay, HIGH);
    delay(7000);
    digitalWrite(pumpoff_relay, LOW);
    pumpStatus = 0;
    String smsPumpMessage = "Pump OFF";
    smsAckSend(smsPumpMessage, buffer);
    delay(10);
    digitalWrite(RED_LED, LOW);
  }
}


//return "error: unable to run AT+CMGR";

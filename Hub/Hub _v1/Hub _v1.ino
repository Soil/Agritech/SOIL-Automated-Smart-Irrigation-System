
/*----------------------Hub program----------------------------------------------*/
#include "time.h"
#define CEST 2*60*60

#include "RTC_Library.h"

// Prototypes
char * datestamp = __DATE__;
char * timestamp = __TIME__;

// Define variables and constants
time_t myEpochRTC;
tm myTimeRTC;

DateTime myRTC;

const int solenoid_relay =  40;

const int pumpon_relay  =  39;
const int pumpoff_relay =  38;

const int phase = 37;
int phaseState = 0;

unsigned int N, count, i, t;
char MyByte[30];
char ch;

unsigned int v, cc;

int j, k;
int pumpSwitch;

/*------------------------setup function-----------------------------------------*/
void setup()
{
  pinMode(phase, INPUT_PULLUP);

  pinMode(solenoid_relay, OUTPUT);
  pinMode(pumpon_relay, OUTPUT);
  pinMode(pumpoff_relay, OUTPUT);

  digitalWrite(solenoid_relay, LOW);
  digitalWrite(pumpon_relay, LOW);
  digitalWrite(pumpoff_relay, LOW);

  // initialize both serial ports:
  Serial.begin(9600);
  Serial1.begin(9600);

}

/*-------------------loop function-------------------------------------------------------------*/
void loop()
{
  t = 0;

  while (1)
  {

    //gsmstart();
    //gsmconcection();
    sensorread();
    //gsmterminate();

    solenoid();
    pump();

    for (k = 0; k < 28; k++)
    {
      for (j = 0; j < 60; j++)
      {
        if (phaseState == 1)
        {
          t = 0;
          digitalWrite(solenoid_relay, LOW);
        }
        delay(1000);
      }
    }
  }
}

/*----------------------gsm start function------------------------------------------*/
void gsmstart()
{
  delay(10000);

  Serial1.print("AT\r\n");
  delay(4000);

  Serial1.print("AT+CIPSHUT\r\n");
  delay(4000);

  Serial1.print("AT+CIPMUX=0\r\n");
  delay(4000);

  Serial1.print("AT+CGATT=1\r\n");
  delay(4000);

  Serial1.print("AT+CSTT=\"www\",\"\",\"\"\r\n");
  delay(4000);

  Serial1.print("AT+CIICR\r\n");
  delay(8000);

  Serial1.print("AT+CIFSR\r\n");
  delay(4000);

}

/*--------------------------gsm connection function-------------------------------------------*/
void gsmconcection()
{
  Serial1.print("AT+CIPSTART=\"TCP\",\"207.154.244.63\",\"80\"\r\n");
  delay(15000);

  Serial1.print("AT+CIPSEND=66\r\n");
  delay(4000);

  Serial1.print("GET /add-sensor-data/giriwar/pass12/1/1/1/");
}

/*----------------------sensor read function--------------------------------------------------*/


void sensorread()
{
  i = 0; N = 19;
  count = 8000;
  Serial.print("D");
  while (i < N && count > 0)
  {
    if (Serial.available())
    {
      ch = Serial.read();
      MyByte[i] = ch;
      i++;
      count = 8000;
    }
    else {
      delay(1);
      count--;
    }
  }

  if (i == 19)
  {
    for (i = 0; i < 19; i++) {
      Serial1.print( MyByte[i]);
    }
  }
  else
  {
    
    Serial1.print("eee,eee,eee,eee,eee");
  }
}



void moistureTest()
{
  if (i = 19)
  {
    MS1dec = ((MyByte[0] * 100) + (MyByte[1] * 10) + (MyByte[2] * 100))
             MS2dec = ((MyByte[3] * 100) + (MyByte[4] * 10) + (MyByte[5] * 100))
                      MSdec = (MSdec1 + MSdec2) / 2
  }

  if (MSdec < 30)

  }

/*-------------------solenoid function---------------------------------------------------------*/

void solenoid()
{
  Serial1.print("AT+CIPSTART=\"TCP\",\"207.154.244.63\",\"80\"\r\n");
  delay(15000);

  Serial1.print("AT+CIPSEND=44\r\n");
  delay(4000);

  Serial1.print("GET /solenoid-status/giriwar/pass12/1/1/1");

  Serial1.print("\r\n");
  delay(1000);

  Serial1.print(0x1a);

  MyByte[0] = '0'; MyByte[1] = '0';
  cc = 0; v = 0;

  phaseState = digitalRead(phase);

  while (v == 0 && cc < 4000)
  {
    while (Serial1.available() == 0 && cc < 4000) {
      delay(1);
      cc++;
    }
    MyByte[0] = Serial1.read();

    if (MyByte[0] == 'O') {
      MyByte[1] = Serial1.read();
    }
    delay(1); cc++;

    if (phaseState == 0)
    {
      //if(MyByte[0]=='O' && MyByte[1]=='N') { digitalWrite(solenoid_relay, HIGH); v=1; }
      //if(MyByte[0]=='O' && MyByte[1]=='F') { digitalWrite(solenoid_relay, LOW);  v=1; }
      if (pumpSwitch == 1) {
        digitalWrite(solenoid_relay, HIGH);
        v = 1;
      }
      if (pumpSwitch == 0) {
        digitalWrite(solenoid_relay, LOW);
        v = 1; tr
      }
    }
    else
    {
      digitalWrite(solenoid_relay, LOW);  v = 1;
    }
  }

  delay(10000);
}

/*------------------------pump function-------------------------------------------------------------*/
void pump()
{
  /*
    Serial1.print("AT+CIPSTART=\"TCP\",\"207.154.244.63\",\"80\"\r\n");
    delay(15000);

    Serial1.print("AT+CIPSEND=40\r\n");
    delay(4000);

    Serial1.print("GET /pump-status/giriwar/pass12/1/1/2");

    Serial1.print("\r\n");
    delay(1000);

    Serial1.print(0x1a);

    MyByte[0]='0'; MyByte[1]='0';
    cc=0; v=0;

    phaseState = digitalRead(phase);

    while(v==0 && cc<4000)
    {
    while(Serial1.available()==0 && cc<4000){ delay(1); cc++; }
    MyByte[0] = Serial1.read();

    if(MyByte[0]=='O'){ MyByte[1] = Serial1.read(); }
    delay(1); cc++;

  */

  if (phaseState == 0)
  {
    //if(MyByte[0]=='O' && MyByte[1]=='N' && t==0) { digitalWrite(pumpon_relay, HIGH);  delay(2500); digitalWrite(pumpon_relay, LOW); t=1; v=1;   }
    //if(MyByte[0]=='O' && MyByte[1]=='F' && t==1) { digitalWrite(pumpoff_relay, HIGH); delay(7000); digitalWrite(pumpoff_relay, LOW);  t=0; v=1; }
    if (pumpSwitch == 1 && t == 0)
    {
      digitalWrite(pumpon_relay, HIGH);
      delay(2500);
      digitalWrite(pumpon_relay, LOW);
      t = 1;
      v = 1;
    }
    if (pumpSwitch == 0 && t == 1)
    {
      digitalWrite(pumpoff_relay, HIGH);
      delay(7000);
      digitalWrite(pumpoff_relay, LOW);
      t = 0;
      v = 1;
    }
  }
  else
  {
    if (t == 1)
    {
      digitalWrite(pumpoff_relay, HIGH);
      delay(7000);
      digitalWrite(pumpoff_relay, LOW);
      t = 0;
      v = 1;
    }
  }
  // }

  delay(1000);
}

/*-------------------------gsm terminate function---------------------------------------------*/
void gsmterminate()
{
  Serial1.print("/00\r\n");
  delay(1000);

  Serial1.print(0x1a);
  delay(10000);
}


void rtc()
{

  timeNow = millis() / 1000; // the number of milliseconds that have passed since boot
  seconds = timeNow - timeLast;//the number of seconds that have passed since the last time 60 seconds was reached.



  if (seconds == 60)
  {
    timeLast = timeNow;
    minutes = minutes + 1;
  }

  //if one minute has passed, start counting milliseconds from zero again and add one minute to the clock.

  if (minutes == 60)
  {
    minutes = 0;
    hours = hours + 1;
  }

  // if one hour has passed, start counting minutes from zero and add one hour to the clock

  if (hours == 24)
  {
    hours = 0;
    days = days + 1;
  }

  //if 24 hours have passed , add one day

  if (hours == (24 - startingHour) && correctedToday == 0)
  {
    delay(dailyErrorFast * 1000);
    seconds = seconds + dailyErrorBehind;
    correctedToday = 1;
  }

  //every time 24 hours have passed since the initial starting time and it has not been reset this day before, add milliseconds or delay the progran with some milliseconds.
  //Change these varialbes according to the error of your board.
  // The only way to find out how far off your boards internal clock is, is by uploading this sketch at exactly the same time as the real time, letting it run for a few days
  // and then determine how many seconds slow/fast your boards internal clock is on a daily average. (24 hours).

  if (hours == 24 - startingHour + 2)
  {
    correctedToday = 0;
  }

  //let the sketch know that a new day has started for what concerns correction, if this line was not here the arduiono
  // would continue to correct for an entire hour that is 24 - startingHour.



  Serial.print("The time is:           ");
  Serial.print(days);
  Serial.print(":");
  Serial.print(hours);
  Serial.print(":");
  Serial.print(minutes);
  Serial.print(":");
  Serial.println(seconds);

}


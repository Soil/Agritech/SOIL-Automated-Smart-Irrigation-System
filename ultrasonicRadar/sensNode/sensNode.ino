// Includes the Servo library
#include <Servo.h>


const int mosituresensor1 = 6;

unsigned int MS1 = 0;


int mydata[3];

int counter = 0;

// Defines Tirg and Echo pins of the Ultrasonic Sensor
const int trigPin = 9;
const int echoPin = 10;

// Variables for the duration and the distance
long duration;
int distance;

Servo myServo; // Creates a servo object for controlling the servo motor

int i = 15;
int j = 15;

int sampleCount = 0;

int sectorCount = 0;
char cmd;


void setup() {
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  Serial.begin(9600);
  myServo.attach(7); // Defines on which pin is the servo motor attached
}


void loop()
{

  if (Serial.available())              // wait for hardware UART
  {
    cmd = Serial.read();
    if (cmd == 'A')
    {
      sensorRead();
    }

    if (cmd == 'B')
    {
      moistRead();
    }
  }

  if (counter == 1) {
    sensorRead();
  }

  //Serial.println("");
  //Serial.println("new func call");
  //sensorRead();
  //delay(2000);



  while (Serial.available() == 0)
  {
    //digitalWrite(GREEN_LED, HIGH);
  }

  //digitalWrite(GREEN_LED, LOW);
}


void sensorRead() {


  // rotates the servo motor from 15 to 165 degrees
  if (counter == 0) {

    while (j == 15 && i < 165) {
      //while (i <= 165) {
      //delay(2000);
      myServo.write(i);
      delay(30);
      distance = calculateDistance();// Calls a function for calculating the distance measured by the Ultrasonic sensor for each degree

      if (distance > 99) {
        distance = 99;
      }

      delay(50);

      /*
        if (sampleCount == 4) {
        delay(200);
        sampleCount = 0;
        }

        sampleCount++;
      */

      if (sectorCount == 50) {
        //delay(200);
        sectorCount = 0;
        //Serial.println("");
        //Serial.println("sector end");
        return;
      }

      sectorCount++;

      if (i == 15 || i == 65 || i == 115) {

        if (i == 15) {
          Serial.print("a");
        }

        if (i == 65) {
          Serial.print("b");
        }

        if (i == 115) {
          Serial.print("c");
        }
      }

      else {
        Serial.print("d");
      }

      if (i < 100) {
        Serial.print("0");
      }
      Serial.print(i); // Sends the current degree into the Serial Port
      Serial.print("m"); // Sends addition character right next to the previous value needed later in the Processing IDE for indexing
      if (distance < 10) {
        Serial.print("0");
      }
      Serial.print(distance); // Sends the distance value into the Serial Port
      //Serial.print("."); // Sends addition character right next to the previous value needed later in the Processing IDE for indexing
      i++;

      if (i == 165) {
        //if (i == 166) {
        j = 165;
        sectorCount = 0;
        counter++;
        //myServo.write(15);
        //delay(100);
      }
    }
  }


  // Repeats the previous lines from 165 to 15 degrees
  else {

    while (i == 165 && j > 15) {
      //delay(2000);
      myServo.write(j);
      delay(30);
      /*
        distance = calculateDistance();

        if (distance > 99) {
        distance = 99;
        }

        delay(50);

        if (sampleCount == 4) {
        delay(200);
        sampleCount = 0;
        }


        sampleCount++;


        if (sectorCount == 50) {
        //delay(200);
        sectorCount = 0;
        //Serial.println("");
        //Serial.println("sector end");
        return;
        }

        sectorCount++;


        if (i = 165) {
        Serial.print("a");
        }

        if (i = 115) {
        Serial.print("b");
        }

        if (i = 65) {
        Serial.print("c");
        }


        //else {
        Serial.print("d");
        //}

        if (j < 100) {
        Serial.print("0");
        }
        Serial.print(j);
        Serial.print("s");
        if (distance < 10) {
        Serial.print("0");
        }
        Serial.print(distance);
        //Serial.print(".");
      */

      j--;

      if (j == 15) {
        i = 15;
        sectorCount = 0;
        counter--;
      }
    }
  }

}


// Function for calculating the distance measured by the Ultrasonic sensor
int calculateDistance() {

  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH); // Reads the echoPin, returns the sound wave travel time in microseconds
  distance = duration * 0.034 / 2;
  return distance;
}


void moistRead()
{
  int i;
  int mositure1value;
  //digitalWrite(LED, HIGH);
  //Serial.println("in serialRead");
  //delay(10);

  for (i = 0; i < 3; i++)
  {
    mydata[i] = 's';
    //Serial.println("in first for loop");
    //delay(10);
  }

  //Serial.println("moisture 1 reading");
  //delay(10);
  // mositure sensor 1
  mositure1value = analogRead(mosituresensor1);
  //Serial.print("analog moist1 =");
  //delay(500);
  //Serial.println(mositure1value);
  //delay(500);

  MS1 = mositure1value;
  
  if (MS1 < 380) {
    MS1 = 380;
  }
  
  if (MS1 > 800) {
    MS1 = 800;
  }

  MS1 = (MS1 - 380) * 10;
  MS1 = 100 - (MS1 / 42);

  if (MS1 > 999) {
    MS1 = 0;
  }

  mydata[0] = MS1 / 100;
  mydata[1] = (MS1 - (mydata[0] * 100)) / 10;
  mydata[2] = MS1 - ((mydata[0] * 100) + mydata[1] * 10 );


  //mydata[0] = 0;
  //mydata[1] = 0;
  //mydata[2] = 0;

  //Serial.print("moisture 1 =");
  //delay(500);
  //Serial.print(mydata[0]);
  //delay(500);
  //Serial.print(mydata[1]);
  //delay(500);
  //Serial.println(mydata[2]);

  /*
  mositure2value = analogRead(mosituresensor2);


  /*
    Serial.print("analog moist2 =");
    delay(500);
    Serial.println(mositure2value);
    delay(500);
  */

  
  /*
  MS1 = mositure2value;
  if (MS1 < 6360) {
    MS1 = 6360;
  }
  if (MS1 > 13000) {
    MS1 = 13000;
  }

  MS1 = (MS1 - 6360) * 10;
  MS1 = 100 - (MS1 / 665);
  */
  //if (MS1 > 999) {
  //  MS1 = 0;
  //}
  /*
  mydata[3] = MS1 / 100;
  mydata[4] = (MS1 - (mydata[3] * 100)) / 10;
  mydata[5] = MS1 - ((mydata[3] * 100) + mydata[4] * 10 );
  */
  //mydata[3] = 0;
  //mydata[4] = 0;
  //mydata[5] = 0;
  
  /*
    Serial.print("moisture 2 =");
    delay(500);
    Serial.print(mydata[3]);
    delay(500);
    Serial.print(mydata[4]);
    delay(500);
    Serial.println(mydata[5]);


    delay(100);
  */
  
  /*
  MS2 = mositure2value;

  mydata[6] = MS2 / 10000;
  mydata[7] = (MS2 - (mydata[6] * 10000)) / 1000;
  mydata[8] = (MS2 - (mydata[6] * 10000 + mydata[7] * 1000)) / 100;
  mydata[9] = (MS2 - (mydata[6] * 10000 + mydata[7] * 1000 + mydata[8] * 100)) / 10;
  mydata[10] = MS2 - (mydata[6] * 10000 + mydata[7] * 1000 + mydata[8] * 100 + mydata[9] * 10);
  */

  //mydata[6] = 0;
  //mydata[7] = 0;
  //mydata[8] = 0;
  //mydata[9] = 0;
  //mydata[10] = 0;

  /*
    Serial.println("mydata analog =");
    delay(500);
    Serial.println(mydata[6]);
    delay(500);
    Serial.println(mydata[7]);
    delay(500);
    Serial.println(mydata[8]);
    delay(500);
    Serial.println(mydata[9]);
    delay(500);
    Serial.println(mydata[10]);
    delay(500);
  */



  //mydata[9] = 0;
  //mydata[10] = 0;
  //mydata[11] = 0;
  //mydata[12] = 0;
  //mydata[13] = 0;
  //mydata[14] = 0;
  //digitalWrite(1, LOW);
  //Serial.println("before forming data");
  //Serial.print(mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]mydata[0]);
  //digitalWrite(LED, LOW);
  Serial.print("x");
  for (i = 0; i < 3; i++)
  {
    //Serial.println("forming data packet");
    //delay(500);
    //Serial.println("i =");
    //Serial.println(i);
    //delay(500);
    Serial.print(mydata[i]);
    //delay(500);
    //Serial.println("data =");
    //Serial.println(mydata[i]);
    //delay(500);
    //if (i == 2 || i == 5 || i == 8 || i == 11)
    //{
      //Serial.print(",");
    //}

    //Serial1.print(mydata[i]);
  }
}

